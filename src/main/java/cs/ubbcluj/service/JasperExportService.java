package cs.ubbcluj.service;

import cs.ubbcluj.model.jasper.ClusterSihouette;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Service
public class JasperExportService {

    public static final String CLUSTERS = "clusters";

    public Path exportFile(ClusterSihouette clusterSihouette, Boolean pdf) throws IOException, JRException {
        String templatePath="jasper\\compiled\\clusterTemplate.jasper";
        if (pdf)
            return exportToPdf(clusterSihouette,templatePath);
        return exportToXlsx(clusterSihouette,templatePath);
    }

    public Path exportToPdf(ClusterSihouette clusterSihouette,String templatePath) throws JRException, IOException {
        Path path=Files.createTempFile(CLUSTERS,".pdf");
        File file=path.toFile();
        file.deleteOnExit();
        try (OutputStream outputStream = new FileOutputStream(file)) {
            Map<String,Object> params=new HashMap<>();
            params.put("Silhouette",clusterSihouette.getSilhouetteCoefficient());
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new ClassPathResource(templatePath).getURL());
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(clusterSihouette.getClusterInstances());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
            return path;
        }
    }

    public Path exportToXlsx(ClusterSihouette clusterSihouette,String templatePath) throws JRException, IOException {
        Path path = Files.createTempFile(CLUSTERS, ".xlsx");
        File file=path.toFile();
        file.deleteOnExit();
        Map<String,Object> params=new HashMap<>();
        params.put("Silhouette",clusterSihouette.getSilhouetteCoefficient());
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject( new ClassPathResource(templatePath).getURL());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(clusterSihouette.getClusterInstances());
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));

        SimpleXlsxReportConfiguration xlsxReportConfiguration= new SimpleXlsxReportConfiguration();
        xlsxReportConfiguration.setOnePagePerSheet(false);
        xlsxReportConfiguration.setRemoveEmptySpaceBetweenRows(true);
        xlsxReportConfiguration.setDetectCellType(true);
        xlsxReportConfiguration.setAutoFitPageHeight(true);
        xlsxReportConfiguration.setShrinkToFit(true);
        xlsxReportConfiguration.setWhitePageBackground(false);
        exporter.setConfiguration(xlsxReportConfiguration);

        exporter.setConfiguration(xlsxReportConfiguration);
        exporter.exportReport();
        return path;
    }
}
