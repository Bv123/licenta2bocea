package cs.ubbcluj.service;

import cs.ubbcluj.model.similarity.DataSetCharacteristics;
import cs.ubbcluj.model.similarity.ProgramDescr;
import cs.ubbcluj.repository.DataSetCharacteristicsRepository;
import cs.ubbcluj.repository.ProgramDescprRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weka.core.Attribute;
import weka.core.Instances;

import java.util.*;

@Service
public class DataSetCharacteristicsService {

    @Autowired
    private ProgramDescprRepository programDescprRepository;

    @Autowired
    private DataSetCharacteristicsRepository dataSetCharacteristicsRepository;

    public DataSetCharacteristics charactersticsForNumericAndNominalDataset(Instances dataSet) {

        DataSetCharacteristics dataSetCharacteristics = new DataSetCharacteristics();
        Map<Object, Integer> frequencyMap = new HashMap<>();

        int[] totalValueNumbers = new int[dataSet.numAttributes()];

        double[] skewnessForAttributes = new double[dataSet.numAttributes()];
        double[] stdDevForAttributes = new double[dataSet.numAttributes()];
        double[] kurtosisForAttributes = new double[dataSet.numAttributes()];
        double[] meanAttributes = new double[dataSet.numAttributes()];
        double[] attibuteTypes = new double[3];

        Double skewnessSum = 0D, kurtosisSum = 0D, stdDevSum = 0D;
        List<List<Double>> numericAttributeValues = new ArrayList<>();

        preprocessNumericAndNominalDataSet(dataSet, frequencyMap, totalValueNumbers, meanAttributes, attibuteTypes, numericAttributeValues);

        for (int i = 0; i < dataSet.numAttributes(); ++i) {
            final int counter = i;
            Attribute attribute = dataSet.attribute(i);
            if (attribute.isNumeric()) {
                Double mean = meanAttributes[i] / dataSet.numInstances();
                numericAttributeValues.get(i).forEach(attributeValue -> {
                    Double diff = attributeValue - mean;
                    skewnessForAttributes[counter] += Math.pow(diff, 3) / dataSet.numInstances();
                    kurtosisForAttributes[counter] += Math.pow(diff, 4) / dataSet.numInstances();
                    stdDevForAttributes[counter] += Math.pow(diff, 2);
                });
                stdDevForAttributes[counter] = Math.sqrt((1.0 / dataSet.numInstances()) * stdDevForAttributes[counter]);
            } else if (attribute.isNominal()) {
                double sum = 0;
                int nrVals = dataSet.attribute(i).numValues();
                for (int j = 0; j < nrVals; ++j) {
                    String value = dataSet.attribute(i).value(j);
                    if (frequencyMap.containsKey(value)) {
                        sum += ((double) frequencyMap.get(value)) / totalValueNumbers[i];
                    }
                }
                meanAttributes[i] = sum / nrVals;
                for (int j = 0; j < nrVals; ++j) {
                    double frequency = 0;
                    if (frequencyMap.containsKey(dataSet.attribute(i).value(j))) {
                        frequency = ((double) frequencyMap.get(dataSet.attribute(i).value(j))) / totalValueNumbers[i];
                    }
                    Double diff = frequency - meanAttributes[i];
                    stdDevForAttributes[i] += Math.pow(diff, 2);
                    skewnessForAttributes[i] += Math.pow(diff, 3) / nrVals;
                    kurtosisForAttributes[i] += Math.pow(diff, 4) / nrVals;
                }
                stdDevForAttributes[i] = Math.sqrt((1.0 / nrVals) * stdDevForAttributes[i]);
            }

            if (stdDevForAttributes[counter] != 0) {
                skewnessForAttributes[counter] = skewnessForAttributes[counter] / Math.pow(stdDevForAttributes[counter], 3);
                kurtosisForAttributes[counter] = kurtosisForAttributes[counter] / Math.pow(stdDevForAttributes[counter], 4);
            } else {
                skewnessForAttributes[counter] = 0;
                kurtosisForAttributes[counter] = 0;
            }
            skewnessSum += skewnessForAttributes[counter];
            kurtosisSum += kurtosisForAttributes[counter];
            stdDevSum += stdDevForAttributes[counter];
        }
        setPropertiesToDataSetCharacteristics(dataSet, dataSetCharacteristics, frequencyMap, skewnessForAttributes, kurtosisForAttributes, attibuteTypes, skewnessSum, kurtosisSum, stdDevSum);
        return dataSetCharacteristics;
    }

    private void preprocessNumericAndNominalDataSet(Instances dataSet, Map<Object, Integer> frequencyMap, int[] totalValueNumbers, double[] meanAttributes, double[] attibuteTypes, List<List<Double>> numericAttributeValues) {
        dataSet.forEach(x -> {
            for (int i = 0; i < x.numAttributes(); ++i) {
                numericAttributeValues.add(new ArrayList<>());
                if (x.attribute(i).isNominal()) {
                    ++attibuteTypes[0];
                    ++totalValueNumbers[i];
                    String value = dataSet.attribute(i).value((int) x.value(i));
                    if (frequencyMap.containsKey(value))
                        frequencyMap.put(value, frequencyMap.get(value) + 1);
                    else frequencyMap.put(value, 1);
                } else if (x.attribute(i).isNumeric()) {
                    ++attibuteTypes[1];
                    double value = x.value(i);
                    if (frequencyMap.containsKey(value))
                        frequencyMap.put(value, frequencyMap.get(value) + 1);
                    else frequencyMap.put(value, 1);
                    if (value == 1 || value == 0) {
                        ++attibuteTypes[2];
                    }
                    numericAttributeValues.get(i).add(value);
                    meanAttributes[i] += value;
                }
            }
        });
    }

    private void setPropertiesToDataSetCharacteristics(Instances dataSet, DataSetCharacteristics dataSetCharacteristics, Map<Object, Integer> frequencyMap, double[] skewnessForAttributes, double[] kurtosisForAttributes, double[] attibuteTypes, Double skewnessSum, Double kurtosisSum, Double stdDevSum) {
        Integer numberOfValues = dataSet.numInstances() * dataSet.numAttributes();
        dataSetCharacteristics.setPercentNumAttr(attibuteTypes[1] / numberOfValues);
        dataSetCharacteristics.setPercentNomAttr(attibuteTypes[0] / numberOfValues);
        dataSetCharacteristics.setPercentBinAttr(attibuteTypes[2] / numberOfValues);
        dataSetCharacteristics.setSkewness(computeStandardDeviation(skewnessForAttributes));
        dataSetCharacteristics.setSkewnessMean(skewnessSum / dataSet.numAttributes());
        dataSetCharacteristics.setKurtosis(computeStandardDeviation(kurtosisForAttributes));
        dataSetCharacteristics.setKurtosisMean(kurtosisSum / dataSet.numAttributes());
        dataSetCharacteristics.setAttributeType(3);
        dataSetCharacteristics.setStandardDeviation(stdDevSum / dataSet.numAttributes());
        dataSetCharacteristics.setEntropy(computeEntropy(frequencyMap, dataSet.numAttributes() * dataSet.numInstances()));
        dataSetCharacteristics.setNumAttr(dataSet.numAttributes());
    }

    public DataSetCharacteristics charactersticsForNominalDataset(Instances dataSet) {

        DataSetCharacteristics dataSetCharacteristics = new DataSetCharacteristics();
        Map<Object, Integer> frequencyMap = new HashMap<>();

        int[] totalValueNumbers = new int[dataSet.numAttributes() + 1];

        double[] skewnessForAttributes = new double[dataSet.numAttributes()];
        double[] kurtosisForAttributes = new double[dataSet.numAttributes()];

        preprocessNominalDataSet(dataSet, frequencyMap, totalValueNumbers);

        Double skewnessSum = 0D, kurtosisSum = 0D, stdDevSum = 0D;
        for (int i = 0; i < dataSet.numAttributes(); ++i) {
            int nrVals = dataSet.attribute(i).numValues();
            double sum = 0, mean = 0, stdDev = 0, skewness = 0, kurtosis = 0;
            //summ of frequencies = 1
            for (int j = 0; j < nrVals; ++j) {
                String value = dataSet.attribute(i).value(j);
                if (frequencyMap.containsKey(value)) {
                    sum += ((double) frequencyMap.get(value)) / totalValueNumbers[i];
                }
            }
            mean = sum / nrVals;
            for (int j = 0; j < nrVals; ++j) {
                double frequency = 0;
                if (frequencyMap.containsKey(dataSet.attribute(i).value(j))) {
                    frequency = ((double) frequencyMap.get(dataSet.attribute(i).value(j))) / totalValueNumbers[i];
                }
                Double diff = frequency - mean;
                stdDev += Math.pow(diff, 2);
                skewness += Math.pow(diff, 3) / nrVals;
                kurtosis += Math.pow(diff, 4) / nrVals;
            }
            stdDev = Math.sqrt((1.0 / nrVals) * stdDev);
            if (stdDev != 0) {
                skewness = skewness / Math.pow(stdDev, 3);
                kurtosis = kurtosis / Math.pow(stdDev, 4);
            } else {
                skewness = 0;
                kurtosis = 0;
            }
            //    entropySum += computeEntropy(frequencyMap, dataSet.attribute(i));
            kurtosisForAttributes[i] = kurtosis;
            skewnessForAttributes[i] = skewness;
            skewnessSum += skewness;
            kurtosisSum += kurtosis;
            stdDevSum += stdDev;
        }

        setPropertiesToNominalDataSetCharacteristics(dataSet, dataSetCharacteristics, computeEntropy(frequencyMap, totalValueNumbers[dataSet.numAttributes()]), skewnessForAttributes, kurtosisForAttributes, skewnessSum / dataSet.numAttributes(), kurtosisSum / dataSet.numAttributes(), stdDevSum / dataSet.numAttributes());
        return dataSetCharacteristics;
    }

    private void setPropertiesToNominalDataSetCharacteristics(Instances dataSet, DataSetCharacteristics dataSetCharacteristics, Double entropy, double[] skewnessForAttributes, double[] kurtosisForAttributes, double skewnessMean, double kurtosisMean, double standardDeviation) {
        dataSetCharacteristics.setSkewnessMean(skewnessMean);
        dataSetCharacteristics.setKurtosisMean(kurtosisMean);
        dataSetCharacteristics.setEntropy(entropy);
        dataSetCharacteristics.setSkewness(computeStandardDeviation(skewnessForAttributes));
        dataSetCharacteristics.setKurtosis(computeStandardDeviation(kurtosisForAttributes));
        dataSetCharacteristics.setPercentBinAttr(0D);
        dataSetCharacteristics.setPercentNumAttr(0D);
        dataSetCharacteristics.setPercentNomAttr(1D);
        dataSetCharacteristics.setNumAttr(dataSet.numAttributes());
        dataSetCharacteristics.setAttributeType(1);
        dataSetCharacteristics.setStandardDeviation(standardDeviation);
    }

    private void preprocessNominalDataSet(Instances dataSet, Map<Object, Integer> frequencyMap, int[] totalValueNumbers) {
        dataSet.forEach(x -> {
            for (int i = 0; i < x.numAttributes(); ++i) {
                ++totalValueNumbers[i];
                ++totalValueNumbers[x.numAttributes()];
                String value = dataSet.attribute(i).value((int) x.value(i));
                if (frequencyMap.containsKey(value))
                    frequencyMap.put(value, frequencyMap.get(value) + 1);
                else frequencyMap.put(value, 1);
            }
        });
    }

    public DataSetCharacteristics charactersticsForDataset(Instances dataSet) {
        DataSetCharacteristics dataSetCharacteristics = new DataSetCharacteristics();
        double[] skewnessForInstance = new double[dataSet.numInstances()];
        double[] kurtosisForInstance = new double[dataSet.numInstances()];
        //1 num attr 2 nom att 3 total attr 4 skewness 5 kurtosis  7 bin attr
        Map<Object, Integer> frequencyMap = new HashMap<>();
        double[] values = {0, 0, 0, 0, 0, 0, 0};
        double[] stdInfo = {0, 0};
        final int[] counter = {0};
        List<Double> numeric = new ArrayList<>();

        processNumericalDataSet(dataSet, skewnessForInstance, kurtosisForInstance, frequencyMap, values, stdInfo, counter, numeric);

        if (values[5] > 0) {
            dataSetCharacteristics.setSkewnessMean(values[3] / values[5]);
            dataSetCharacteristics.setKurtosisMean(values[4] / values[5]);
        }
        if (stdInfo[1] > 0) {
            stdInfo[0] = stdInfo[0] / stdInfo[1];
            double sum = numeric.stream().mapToDouble(x -> Math.pow(x - stdInfo[0], 2)).sum();
            dataSetCharacteristics.setStandardDeviation(Math.sqrt((1.0 / stdInfo[1]) * sum));
        }

        setCharacteristicsToNumericalDataSetCharacteristics(dataSet, dataSetCharacteristics, skewnessForInstance, kurtosisForInstance, frequencyMap, values);
        return dataSetCharacteristics;
    }

    private void setCharacteristicsToNumericalDataSetCharacteristics(Instances dataSet, DataSetCharacteristics dataSetCharacteristics, double[] skewnessForInstance, double[] kurtosisForInstance, Map<Object, Integer> frequencyMap, double[] values) {
        dataSetCharacteristics.setSkewness(computeStandardDeviation(skewnessForInstance));
        dataSetCharacteristics.setKurtosis(computeStandardDeviation(kurtosisForInstance));
        dataSetCharacteristics.setNumAttr(dataSet.numAttributes());
        if (values[0] > 0) dataSetCharacteristics.setPercentBinAttr(values[6] / values[0]);
        dataSetCharacteristics.setEntropy(computeEntropy(frequencyMap, values[2]));
        dataSetCharacteristics.setPercentNumAttr((values[2] == 0) ? 0 : values[0] / values[2]);
        dataSetCharacteristics.setPercentNomAttr((values[2] == 0) ? 0 : values[1] / values[2]);
        dataSetCharacteristics.setAttributeType(2);
    }

    private void processNumericalDataSet(Instances dataSet, double[] skewnessForInstance, double[] kurtosisForInstance, Map<Object, Integer> frequencyMap, double[] values, double[] stdInfo, int[] counter, List<Double> numeric) {
        dataSet.forEach(x -> {
            int nrNumericAttr = 0;
            double mean = 0, std = 0, sum = 0, kurtosisSum = 0, stdSum = 0;
            for (int i = 0; i < x.numAttributes(); ++i) {
                Attribute att = x.attribute(i);
                if (att.isNumeric() && !x.isMissing(att)) {
                    Double value = x.value(i);
                    if (frequencyMap.containsKey(value))
                        frequencyMap.put(value, frequencyMap.get(value) + 1);
                    else frequencyMap.put(value, 1);
                    mean += value;
                    ++nrNumericAttr;
                    stdInfo[0] += value;
                    stdInfo[1] += 1;
                    numeric.add(value);
                } else {
                    String value = x.stringValue(att);
                    if (frequencyMap.containsKey(value)) {
                        frequencyMap.put(value, frequencyMap.get(value) + 1);
                    } else frequencyMap.put(value, 1);
                }
            }
            mean /= nrNumericAttr;
            for (int i = 0; i < x.numAttributes(); ++i) {
                Attribute att = x.attribute(i);
                if (att.isNumeric() && !x.isMissing(att)) {
                    if (x.value(i) == 0 || x.value(i) == 1) {
                        ++values[6];
                    }
                    double diff = x.value(i) - mean;
                    sum += Math.pow(diff, 3) / nrNumericAttr;
                    kurtosisSum += Math.pow(diff, 4) / nrNumericAttr;
                    stdSum += Math.pow(diff, 2);
                    ++values[0];
                } else if (att.isNominal()) {
                    ++values[1];
                }
                ++values[2];
            }
            ++values[5];
            std = Math.sqrt((1.0 / nrNumericAttr) * stdSum);
            Double skewness = 0D, kurtosis = 0D;
            if (std > 0) {
                skewness = (sum / Math.pow(std, 3));
                kurtosis = (kurtosisSum / Math.pow(std, 4));
            }
            values[3] += skewness;
            values[4] += kurtosis;
            skewnessForInstance[counter[0]] = skewness;
            kurtosisForInstance[counter[0]] = kurtosis;
            ++counter[0];
        });
    }

    private Double copmputeSkewness(List<Double> skewnessList) {
        Double mean = skewnessList.stream().mapToDouble(x -> x).sum() / skewnessList.size();
        Double standardDeviation = Math.sqrt(skewnessList.stream().mapToDouble(x -> Math.pow(x - mean, 2)).sum());
        System.out.println(standardDeviation);
        Double standardDev1 = standardDeviation * (1.0 / skewnessList.size());
        System.out.println(standardDev1);
        standardDeviation /= skewnessList.size();
        double a = ((skewnessList.stream().mapToDouble(x -> Math.pow((x - mean), 3) / skewnessList.size()).sum()));
        return (Math.sqrt(skewnessList.size() * (skewnessList.size() - 1)) / (skewnessList.size() - 2)) * (a / Math.pow(standardDeviation, 3));
    }

    private Double computeKurtosis(List<Double> kurtosisList) {
        Double mean = kurtosisList.stream().mapToDouble(x -> x).sum() / kurtosisList.size();
        Double standardDeviation = Math.sqrt(kurtosisList.stream().mapToDouble(x -> Math.pow(x - mean, 2)).sum() * (1 / kurtosisList.size()));
        // System.out.println(standardDeviation);
        double a = ((kurtosisList.stream().mapToDouble(x -> Math.pow((x - mean), 4) / kurtosisList.size()).sum()));
        //  System.out.println(a);
        return a / Math.pow(standardDeviation, 4);
    }

    private Double computeStandardDeviation(double[] values) {
        double sum = 0, mean, stdDev = 0;
        for (double value : values) {
            sum += value;
        }
        mean = sum / values.length;
        for (double value : values) {
            stdDev += Math.pow(value - mean, 2);
        }
        return Math.sqrt((1.0 / values.length) * stdDev);
    }

    private Double computeEntropy(Map<Object, Integer> frequencyMap, double nrAttributes) {
        return -1 * frequencyMap.entrySet().stream().mapToDouble(x -> {
            Double prob = x.getValue() / nrAttributes;
            return prob * ((Math.log(prob) / Math.log(2)));
        }).sum();
    }

    private Double computeEntropy(Map<Object, Integer> frequencyMap, Attribute attribute) {
        Double sum = 0D;
        for (int i = 0; i < attribute.numValues(); ++i) {
            double freq = ((double) frequencyMap.get(attribute.value(i))) / attribute.numValues();
            sum += freq * ((Math.log(freq) / Math.log(2)));
        }
        return -1 * sum;
    }

    public Double percentNumAttr(Instances dataSet) {
        double[] count = {0, 0};
        //TODO refactor to java 8
        Collections.list(dataSet.enumerateAttributes()).forEach(x ->
        {
            if (x.isNumeric())
                ++count[0];
            ++count[1];
        });
        return (count[1] == 0) ? 0 : count[0] / count[1];
    }

    public Double percentNomAttr(Instances dataSet) {
        double[] count = {0, 0};
        //TODO refactor to java 8
        Collections.list(dataSet.enumerateAttributes()).forEach(x ->
        {
            if (x.isNominal())
                ++count[0];
            ++count[1];
        });
        return (count[1] == 0) ? 0 : count[0] / count[1];
    }

    public Double Skewness(Instances dataset) {
        return 0D;
    }

    public DataSetCharacteristics save(DataSetCharacteristics dataSetCharacteristics) {
        return dataSetCharacteristicsRepository.save(dataSetCharacteristics);
    }

    public DataSetCharacteristics findOne(Long id) {
        return dataSetCharacteristicsRepository.getOne(id);
    }

    public List<DataSetCharacteristics> getAll() {
        return dataSetCharacteristicsRepository.findAll();
    }


    public List<ProgramDescr> matchesDataSetCharacteristics(DataSetCharacteristics dataSetCharacteristics, Integer numClusters) {
        return programDescprRepository.matchesDataSetCharacteristics(numClusters, dataSetCharacteristics.getEntropy(), dataSetCharacteristics.getStandardDeviation(), dataSetCharacteristics.getNumAttr(), dataSetCharacteristics.getAttributeType()
                , dataSetCharacteristics.getPercentBinAttr(), dataSetCharacteristics.getPercentNomAttr(), dataSetCharacteristics.getPercentNumAttr(), dataSetCharacteristics.getSkewness(), dataSetCharacteristics.getKurtosis());
    }

    public DataSetCharacteristics exists(DataSetCharacteristics dataSetCharacteristics) {
        return dataSetCharacteristicsRepository.findByAll(dataSetCharacteristics.getEntropy(), dataSetCharacteristics.getStandardDeviation(), dataSetCharacteristics.getNumAttr(), dataSetCharacteristics.getAttributeType()
                , dataSetCharacteristics.getPercentBinAttr(), dataSetCharacteristics.getPercentNomAttr(), dataSetCharacteristics.getPercentNumAttr(), dataSetCharacteristics.getSkewnessMean(), dataSetCharacteristics.getKurtosisMean(), dataSetCharacteristics.getSkewness(), dataSetCharacteristics.getKurtosis());
    }
}
