package cs.ubbcluj.service;

import cs.ubbcluj.model.algorithm.Algorithm;
import cs.ubbcluj.model.jasper.ClusterInstances;
import cs.ubbcluj.model.jasper.ClusterSihouette;
import cs.ubbcluj.model.program.AttributeSelectionStep;
import cs.ubbcluj.model.program.MachineLearningStep;
import cs.ubbcluj.model.program.PreprocessingStep;
import cs.ubbcluj.model.program.Program;
import cs.ubbcluj.model.similarity.DataSetCharacteristics;
import cs.ubbcluj.model.similarity.ProgramDescr;
import cs.ubbcluj.repository.ProgramDescprRepository;
import cs.ubbcluj.request.ClustererRequest;
import cs.ubbcluj.utils.factory.ClustererFactory;
import cs.ubbcluj.utils.factory.DistanceFunctionFactory;
import cs.ubbcluj.utils.factory.EvaluatorFactory;
import cs.ubbcluj.utils.factory.FilterFactory;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AlgorithmService {

    @Autowired
    private DataSetCharacteristicsService dataSetCharacteristicsService;

    @Autowired
    private ProgramDescprRepository programDescprRepository;

    @Autowired
    private ClustererFactory clustererFactory;

    @Autowired
    private DistanceFunctionFactory distanceFunctionFactory;

    @Autowired
    private FilterFactory filterFactory;

    @Autowired
    private EvaluatorFactory evaluatorFactory;

    @Autowired
    private JasperExportService jasperExportService;

    public Map<Integer, List<Instance>> executeAlgorithm(Algorithm algorithm) {
        try {
            return algorithm.execute().getMachineLearningStep().getClusters();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    @Transactional
    public Path executeAlgorithmWithFile(Algorithm algorithm, Boolean pdf, Boolean useKnowledgeBase) throws Exception {
        int attType = checkIfNumericAttributes(algorithm.getDataSet());
        DataSetCharacteristics currentCharactersistics = null;
        if (attType == 1) {
            currentCharactersistics = dataSetCharacteristicsService.charactersticsForNominalDataset(algorithm.getDataSet());
        } else if (attType == 2) {
            currentCharactersistics = dataSetCharacteristicsService.charactersticsForDataset(algorithm.getDataSet());
        } else {
            currentCharactersistics = dataSetCharacteristicsService.charactersticsForNumericAndNominalDataset(algorithm.getDataSet());
        }
        if (useKnowledgeBase) {
            List<ProgramDescr> programDescrs = dataSetCharacteristicsService.matchesDataSetCharacteristics(currentCharactersistics, algorithm.getNumClusters());
            if (!programDescrs.isEmpty()) {
                System.out.println("Used Knowledgebase");
                return createProgram(algorithm, pdf, programDescrs);
            }
        }
        Program program = algorithm.execute();
        String algorithmParam = program.getMachineLearningStep().getClusterer().getClass().getSimpleName();
        String preprocessParam = (program.getPreprocessingStep().getFilter() == null) ? null : program.getPreprocessingStep().getFilter().getClass().getSimpleName();
        String distanceFunctionParam = program.getDistanceFunction().getClass().getSimpleName();
        ProgramDescr programDescrPersisted = programDescprRepository.checkIfProgramDescExists(algorithmParam, preprocessParam, distanceFunctionParam, algorithm.getNumClusters());
        DataSetCharacteristics persistedCharactersistics = dataSetCharacteristicsService.exists(currentCharactersistics);
        if (persistedCharactersistics != null) {
            currentCharactersistics = persistedCharactersistics;
        }
        if (programDescrPersisted == null) {
            saveDataSetCharacteristic(currentCharactersistics, program, algorithmParam, preprocessParam, algorithm.getNumClusters());
        } else {
            currentCharactersistics.getProgramDescr().add(programDescrPersisted);
            dataSetCharacteristicsService.save(currentCharactersistics);
        }
        return exportToFile(pdf, program);
    }

    private int checkIfNumericAttributes(Instances dataSet) {
        boolean numeric = true, nominal = true;
        for (int i = 0; i < dataSet.numAttributes(); ++i) {
            if (dataSet.attribute(i).isNumeric()) {
                nominal = false;
            } else if (dataSet.attribute(i).isNominal()) {
                numeric = false;
            }
        }
        return numeric ? 0 : nominal ? 1 : -1;
    }

    private void saveDataSetCharacteristic(DataSetCharacteristics currentCharactersistics, Program program, String algorithmParam, String preprocessParam, Integer numClusters) {
        ProgramDescr programDescr = new ProgramDescr();
        programDescr.setAlgorithm(algorithmParam);
        programDescr.setNumClusters(numClusters);
        programDescr.setPreprocessing(preprocessParam);
        programDescr.setAttrSelection(null);
        programDescr.setDistanceFunction(program.getDistanceFunction().getClass().getSimpleName());
        programDescr = programDescprRepository.save(programDescr);
        currentCharactersistics.getProgramDescr().add(programDescr);
        dataSetCharacteristicsService.save(currentCharactersistics);
    }

    private Path createProgram(Algorithm algorithm, Boolean pdf, List<ProgramDescr> programDescrs) throws IOException, JRException {
        Optional<Program> best = programDescrs.stream().map(programDescr -> {
            Program program = new Program();
            try {
                createAndExecuteProgram(algorithm, programDescr, program);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return program;
        }).min((a, b) -> a.getFitness() < b.getFitness() ? 1 : 0);
        System.out.println(best.get().getMachineLearningStep().getClusterer().getClass().getSimpleName());
        if (best.get().getPreprocessingStep().getFilter() != null)
            System.out.println(best.get().getPreprocessingStep().getFilter().getClass().getSimpleName());
        System.out.println(best.get().getFitness());
        return exportToFile(pdf, best.orElseThrow(() -> new RuntimeException("Error Occured")));
    }

    private void createAndExecuteProgram(Algorithm algorithm, ProgramDescr programDescr, Program program) throws Exception {
        program.setEvaluation(algorithm.getEvaluator());
        program.setData(algorithm.getDataSet());
        program.setDistanceFunction(distanceFunctionFactory.getObject(programDescr.getDistanceFunction()));
        program.setPreprocessingStep(new PreprocessingStep());
        program.getPreprocessingStep().setFilter(filterFactory.getObject(programDescr.getPreprocessing()));
        program.setMachineLearningStep(new MachineLearningStep());
        ClustererRequest clustererRequest = new ClustererRequest();
        clustererRequest.setNumClusters(algorithm.getNumClusters());
        program.getMachineLearningStep().setClusterer(clustererFactory.getObjectWithParams(programDescr.getAlgorithm(), clustererRequest));
        program.setAtributeSelectionStep(new AttributeSelectionStep());
        program.execute();
    }


    public Path clusterDataToFile(Boolean pdf, MultipartFile data, String clustererName, Integer numClusters, String evaluatorName, String filterName, String distanceFunctionName) throws Exception {
        Program program = createProgram(data, clustererName, numClusters, evaluatorName, filterName, distanceFunctionName);
        System.out.println(program.getMachineLearningStep().getClusterer().getClass().getSimpleName());
        System.out.println(program.getPreprocessingStep().getFilter());
        return exportToFile(pdf, program);
    }

    private Path exportToFile(Boolean pdf, Program program) throws IOException, JRException {
        ClusterSihouette clusterSihouette = new ClusterSihouette();
        clusterSihouette.setClusterInstances(program.getMachineLearningStep().getClusters().entrySet().stream().map(entry -> {
            ClusterInstances clusterInstance = new ClusterInstances();
            clusterInstance.setInteger(entry.getKey());
            clusterInstance.setInstances(entry.getValue());
            return clusterInstance;
        }).collect(Collectors.toList()));
        clusterSihouette.setSilhouetteCoefficient(program.getFitness());
        return jasperExportService.exportFile(clusterSihouette, pdf);
    }

    private Program createProgram(MultipartFile data, String clustererName, Integer numClusters, String evaluatorName, String filterName, String distanceFunctionName) throws Exception {
        ConverterUtils.DataSource dataSource = new ConverterUtils.DataSource(data.getInputStream());
        ClustererRequest request = new ClustererRequest();
        request.setNumClusters(numClusters);
        Program program = new Program();
        program.setData(dataSource.getDataSet());
        program.setEvaluation(evaluatorFactory.getObject(evaluatorName));
        program.setDistanceFunction(distanceFunctionFactory.getObject(distanceFunctionName));
        PreprocessingStep preprocessingStep = new PreprocessingStep();
        preprocessingStep.setFilter(filterFactory.getObject(filterName));
        program.setPreprocessingStep(preprocessingStep);
        AttributeSelectionStep attributeSelectionStep = new AttributeSelectionStep();
        program.setAtributeSelectionStep(attributeSelectionStep);
        MachineLearningStep machineLearningStep = new MachineLearningStep();
        machineLearningStep.setClusterer(clustererFactory.getObjectWithParams(clustererName, request));
        program.setMachineLearningStep(machineLearningStep);
        program.execute();
        return program;
    }
}
