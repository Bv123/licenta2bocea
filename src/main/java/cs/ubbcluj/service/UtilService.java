package cs.ubbcluj.service;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class UtilService {

    public File convertCSVToArff(MultipartFile file) throws IOException {
        CSVLoader loader = new CSVLoader();
        loader.setSource(file.getInputStream());
        Instances data = loader.getDataSet();

        Path path = Files.createTempFile(file.getName(), ".arff");
        File tempFile = path.toFile();
        tempFile.deleteOnExit();
        ArffSaver saver = new ArffSaver();
        saver.setInstances(data);
        saver.setFile(tempFile);
        saver.setDestination(new FileOutputStream(tempFile));
        saver.writeBatch();
        return tempFile;
    }
}
