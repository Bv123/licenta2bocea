package cs.ubbcluj.config;

import cs.ubbcluj.model.algorithm.geneticAlgorithm.GeneticAlgorithm;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;

@Configuration
public class BeanConfiguration {

    @PostConstruct
    public void initObjectMaps() {
        ObjectMaps.initMap();
    }

    @PostConstruct
    public void initExecutorService() {
        GeneticAlgorithm.executorService = Executors.newFixedThreadPool(30);
    }
}
