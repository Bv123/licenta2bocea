package cs.ubbcluj.config;

import cs.ubbcluj.model.clusterers.PAMClusterAlgorithm;
import weka.attributeSelection.*;
import weka.clusterers.*;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.*;
import weka.filters.unsupervised.attribute.PrincipalComponents;
import weka.filters.unsupervised.instance.Resample;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class ObjectMaps {

    private static Map<Integer, Function<Integer, Clusterer>> clusterersMap;
    private static Map<Integer, Supplier<Filter>> filterMap;
    private static Map<Integer, Supplier<ASEvaluation>> evaluatorMap;
    private static Map<Integer, Supplier<ASSearch>> searchMap;

    public static void initMap() {
        clusterersMap = new HashMap<>();
        clusterersMap.put(0, x -> {
            EM a = new EM();
            try {
                a.setNumClusters(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return a;
        });
        clusterersMap.put(1, x -> {
            SimpleKMeans a = new SimpleKMeans();
            try {
                a.setNumClusters(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return a;
        });
        clusterersMap.put(2, x -> {
            Canopy a = new Canopy();
            try {
                a.setNumClusters(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return a;
        });
        clusterersMap.put(3, x -> {
            MakeDensityBasedClusterer a = new MakeDensityBasedClusterer();
            try {
                a.setNumClusters(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return a;
        });
        clusterersMap.put(4, x -> {
            FarthestFirst a = new FarthestFirst();
            try {
                a.setNumClusters(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return a;
        });
        clusterersMap.put(5, x -> {
            PAMClusterAlgorithm a = new PAMClusterAlgorithm();
            try {
                a.setNumClusters(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return a;
        });
        filterMap = new HashMap<>();
        filterMap.put(0, Standardize::new);
        filterMap.put(1, Discretize::new);
        filterMap.put(2, Normalize::new);
        filterMap.put(3, Resample::new);
        filterMap.put(4, PrincipalComponents::new);
        filterMap.put(5, ReplaceMissingValues::new);
        filterMap.put(6, () -> null);
        filterMap.put(7, NominalToBinary::new);

        evaluatorMap = new HashMap<>();
        evaluatorMap.put(0, CfsSubsetEval::new);
        evaluatorMap.put(1, WrapperSubsetEval::new);
        evaluatorMap.put(2, CorrelationAttributeEval::new);
        evaluatorMap.put(3, InfoGainAttributeEval::new);
        evaluatorMap.put(4, OneRAttributeEval::new);
        evaluatorMap.put(5, ReliefFAttributeEval::new);
        evaluatorMap.put(6, SymmetricalUncertAttributeEval::new);

        searchMap=new HashMap<>();
        searchMap.put(0, Ranker::new);
        searchMap.put(1, BestFirst::new);
        searchMap.put(2, GreedyStepwise::new);
    }

    public static Map<Integer, Function<Integer, Clusterer>> getClustererMap() {
        return clusterersMap;
    }

    public static Map<Integer, Supplier<Filter>> getFilterMap() {
        return filterMap;
    }

    public static Map<Integer, Supplier<ASEvaluation>> getEvaluatorMap() {
        return evaluatorMap;
    }

    public static Map<Integer, Supplier<ASSearch>> getSearchMap() {
        return searchMap;
    }
}
