package cs.ubbcluj.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cs.ubbcluj.controller.algorithmParameters.AlgorithmParameter;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.GeneticAlgorithm;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.selection.TournamentSelection;
import cs.ubbcluj.model.evaluation.SilhouetteEvaluator;
import cs.ubbcluj.service.AlgorithmService;
import cs.ubbcluj.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import weka.core.*;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

@RestController
public class FileUploadController {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UtilService utilService;

    public static final String CLUSTERS = "clusters";

    @Autowired
    private AlgorithmService algorithmService;


    @RequestMapping(value = "/cluster", method = RequestMethod.POST)
    public ResponseEntity<ByteArrayResource> exportToFile(@RequestParam("file") MultipartFile multiPartFile,
                                                          @RequestParam("PDF") Boolean pdf,
                                                          @RequestParam("numClusters") Integer numClusters,
                                                          @RequestParam("evaluator") String evaluator,
                                                          @RequestParam("preprocessing") String preprocessing,
                                                          @RequestParam("clusterer") String clusterer,
                                                          @RequestParam("distanceFunction") String distanceFunction) throws Exception {
        Path path = algorithmService.clusterDataToFile(pdf, multiPartFile, clusterer, numClusters, evaluator, preprocessing, distanceFunction);
        return new ResponseEntity<>(new ByteArrayResource(Files.readAllBytes(path)), createHeaders(pdf, path), HttpStatus.OK);
    }

    @RequestMapping(value = "/csvToArff", method = RequestMethod.POST)
    public ResponseEntity<?> csvToArff(@RequestParam("file") MultipartFile file) throws IOException {
        File tempFile = utilService.convertCSVToArff(file);
        HttpHeaders header = new HttpHeaders();
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName() + ".arff");
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.setContentLength(tempFile.length());
        return new ResponseEntity<>(new ByteArrayResource(Files.readAllBytes(tempFile.toPath())), header, HttpStatus.OK);
    }

    @RequestMapping(value = "/cluster/ai", method = RequestMethod.POST)
    public ResponseEntity<ByteArrayResource> exportToFile(@RequestParam("useKnowledgeBase") Boolean useKnowledgeBase, @RequestParam("file") MultipartFile multiPartFile, @RequestParam("algorithmParam")String algorithmParameter, @RequestParam("PDF") Boolean pdf, @RequestParam("numClusters") Integer numClusters, @RequestParam("findOptimumNrClusters") Boolean findOptimumNrClusters) throws Exception {
        ConverterUtils.DataSource dataSource = new ConverterUtils.DataSource(multiPartFile.getInputStream());

        ObjectMapper objectMapper=new ObjectMapper();
        AlgorithmParameter algorithmParameter1=objectMapper.readValue(algorithmParameter,AlgorithmParameter.class);
        System.out.println(algorithmParameter1.getClass());

        Instances data = dataSource.getDataSet();
        GeneticAlgorithm ga = new GeneticAlgorithm();
        ga.setSelection(new TournamentSelection());
        ga.setEvaluator(new SilhouetteEvaluator());
        ga.setNumClusters(numClusters);
        ga.setOriginalData(data);
        DistanceFunction euclideanDistance = new EuclideanDistance();
        ga.setDistanceFunction(euclideanDistance);
        ga.setFindOptimumNumberOfClusters(findOptimumNrClusters);
        Path file = algorithmService.executeAlgorithmWithFile(ga, pdf, useKnowledgeBase);
        return new ResponseEntity<>(new ByteArrayResource(Files.readAllBytes(file)), createHeaders(pdf, file), HttpStatus.OK);
    }

    private HttpHeaders createHeaders(@RequestParam("PDF") Boolean pdf, Path file) {
        String fileName = CLUSTERS;
        if (pdf)
            fileName += ".pdf";
        else fileName += ".xlsx";
        HttpHeaders header = new HttpHeaders();
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.setContentLength(file.toFile().length());
        return header;
    }

    @RequestMapping(value = "/cluster/file", method = RequestMethod.GET)
    public Map<Integer, List<Instance>> clusterWithSetNumberOfClusters(@RequestParam(required = false) String selection, @RequestParam(required = false) String distFunction,
                                                                       @RequestParam(required = false) String evaluator, @RequestParam("numClusters") Integer numClusters,
                                                                       @RequestParam("file") MultipartFile multipartFile) throws Exception {
        ConverterUtils.DataSource dataSource = new ConverterUtils.DataSource(multipartFile.getInputStream());
        Instances data = dataSource.getDataSet();
        GeneticAlgorithm ga = new GeneticAlgorithm();
        ga.setSelection(new TournamentSelection());
        ga.setEvaluator(new SilhouetteEvaluator());
        ga.setNumClusters(numClusters);
        ga.setOriginalData(data);
        DistanceFunction euclideanDistance = new MinkowskiDistance();
        ga.setDistanceFunction(euclideanDistance);
        return algorithmService.executeAlgorithm(ga);
    }
}
