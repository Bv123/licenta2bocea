package cs.ubbcluj.controller;

import cs.ubbcluj.utils.AlgorithmOptionsDTO;
import cs.ubbcluj.utils.enums.ClustererEnum;
import cs.ubbcluj.utils.enums.DistanceFunctionEnum;
import cs.ubbcluj.utils.enums.EvaluatorEnum;
import cs.ubbcluj.utils.enums.FilterEnum;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilController {
    @RequestMapping(value = "/algorithm/options", method = RequestMethod.GET)
    public AlgorithmOptionsDTO getAlgorithmOptions() {
        AlgorithmOptionsDTO algorithmOptions = new AlgorithmOptionsDTO();
        algorithmOptions.setFilters(FilterEnum.values());
        algorithmOptions.setClusterers(ClustererEnum.values());
        algorithmOptions.setDistanceFunctions(DistanceFunctionEnum.values());
        algorithmOptions.setEvaluators(EvaluatorEnum.values());
        return algorithmOptions;
    }
}
