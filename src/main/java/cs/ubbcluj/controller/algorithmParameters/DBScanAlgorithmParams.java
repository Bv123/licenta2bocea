package cs.ubbcluj.controller.algorithmParameters;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("DBScanAlgorithmParam")
public class DBScanAlgorithmParams extends AlgorithmParameter {
    private double epsilon;
    private int minPts;

    public DBScanAlgorithmParams(double epsilon, int minPts) {
        this.epsilon = epsilon;
        this.minPts = minPts;
    }

    public DBScanAlgorithmParams() {
    }

    public double getEpsilon() {
        return epsilon;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }

    public int getMinPts() {
        return minPts;
    }

    public void setMinPts(int minPts) {
        this.minPts = minPts;
    }
}
