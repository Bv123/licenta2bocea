package cs.ubbcluj.controller.algorithmParameters;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("SimpleKmeansParams")
public class SimpleKMeansParams extends AlgorithmParameter {
}
