package cs.ubbcluj.controller.algorithmParameters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DBScanAlgorithmParams.class, name = "DBScanAlgorithmParam"),
        @JsonSubTypes.Type(value = SimpleKMeansParams.class, name = "SimpleKmeansParams")
})
public class AlgorithmParameter {

    @JsonProperty("type")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
