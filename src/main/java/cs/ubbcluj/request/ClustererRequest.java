package cs.ubbcluj.request;

import cs.ubbcluj.request.factory.FactoryRequest;
import cs.ubbcluj.request.mutator.Request;

public class ClustererRequest implements Request,FactoryRequest {
    private Integer numClusters;

    public ClustererRequest() {
    }

    public ClustererRequest(Integer numClusters) {
        this.numClusters = numClusters;
    }

    public Integer getNumClusters() {
        return numClusters;
    }

    public void setNumClusters(Integer numClusters) {
        this.numClusters = numClusters;
    }
}
