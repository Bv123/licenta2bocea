package cs.ubbcluj.model.algorithm.geneticAlgorithm.selection;

import cs.ubbcluj.model.algorithm.geneticAlgorithm.Population;
import cs.ubbcluj.model.program.Program;

import java.util.List;

public interface Selection {
    Program select(Population population) throws Exception;
}
