package cs.ubbcluj.model.algorithm.geneticAlgorithm.selection;

import cs.ubbcluj.model.algorithm.geneticAlgorithm.Population;
import cs.ubbcluj.model.program.Program;

import java.util.Random;

public class RouletteWheelSelection implements Selection {

    private Random random=new Random();

    @Override
    public Program select(Population population) throws Exception {
            double fitnessSum = 0;
            for(Program program:population.getPopulation()) {
                fitnessSum += program.getFitness();
            }
            double value = random.nextDouble() * fitnessSum;
            for(Program program:population.getPopulation()) {
                value -= program.getFitness();
                if(value <= 0) return program;
            }
            return population.getPopulation().get(population.getPopulation().size()-1);
    }
}
