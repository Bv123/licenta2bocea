package cs.ubbcluj.model.algorithm.geneticAlgorithm.mutation;

import cs.ubbcluj.request.mutator.Request;
import cs.ubbcluj.model.program.Step;

public interface Mutator {
    void mutateClustererStepWithParams(Step machineLearningStep, Request request) throws Exception;
    void mutateClustererStep(Step machineLearningStep) throws Exception;
}
