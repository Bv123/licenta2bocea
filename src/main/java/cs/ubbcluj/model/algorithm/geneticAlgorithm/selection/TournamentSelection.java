package cs.ubbcluj.model.algorithm.geneticAlgorithm.selection;

import cs.ubbcluj.model.algorithm.geneticAlgorithm.GAConstants;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.Population;
import cs.ubbcluj.model.program.Program;

import java.util.Random;

public class TournamentSelection implements Selection {

    private Random random;

    public TournamentSelection() {
        random = new Random();
    }

    @Override
    public Program select(Population population) throws Exception {
        Program best=new Program();
        best.setFitness(-2D);
        for (int i = 0; i < GAConstants.TOURNAMENT_SIZE; i++) {
            Program selected=population.getIndivid(random.nextInt(GAConstants.POP_SIZE));
            if(selected.getFitness()>best.getFitness()){
                best=selected;
            }
        }
        return best;
    }
}
