package cs.ubbcluj.model.algorithm.geneticAlgorithm.mutation;

import cs.ubbcluj.request.mutator.Request;
import cs.ubbcluj.config.ObjectMaps;
import cs.ubbcluj.model.program.PreprocessingStep;
import cs.ubbcluj.model.program.Step;
import org.springframework.stereotype.Component;
import weka.filters.Filter;

import java.util.Objects;
import java.util.Random;

@Component
public class PreprocessingMutator implements Mutator {
    private Random random;

    public PreprocessingMutator() {
        random = new Random();
    }

    @Override
    public void mutateClustererStepWithParams(Step preprocessingStep, Request request) throws Exception {
          mutateClustererStep(preprocessingStep);
    }

    @Override
    public void mutateClustererStep(Step preprocessingStep) throws Exception {
        Filter filter =  ObjectMaps.getFilterMap().get(random.nextInt( ObjectMaps.getFilterMap().size())).get();
        if (Objects.nonNull(filter)) {
            ((PreprocessingStep)preprocessingStep).setFilter(filter);
        }
    }

}
