package cs.ubbcluj.model.algorithm.geneticAlgorithm;

import cs.ubbcluj.config.ObjectMaps;
import cs.ubbcluj.model.evaluation.Evaluator;
import cs.ubbcluj.model.featureselection.AttributeSelectionAttributeEvaluator;
import cs.ubbcluj.model.featureselection.AttributeSelectionSubsetEvaluator;
import cs.ubbcluj.model.program.*;
import cs.ubbcluj.utils.factory.DistanceFunctionFactory;
import cs.ubbcluj.utils.factory.EvaluatorFactory;
import weka.core.DistanceFunction;
import weka.core.Instances;
import weka.filters.Filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.function.Supplier;

public class Population {

    private List<Program> population;
    private Random random = new Random();
    private EvaluatorFactory evaluatorFactory = new EvaluatorFactory();
    private DistanceFunctionFactory distanceFunctionFactory = new DistanceFunctionFactory();


    public Population() {
        population = new ArrayList<>();
    }

    public Population(Integer popSize, Evaluator evaluator, Instances data, Integer numClusters, DistanceFunction distanceFunction) throws Exception {
        population = new ArrayList<>();
        for (int i = 0; i < popSize; ++i) {
            Program program = new Program();
            program.setEvaluation(evaluatorFactory.getObject(evaluator.getClass().getSimpleName()));
            program.setDistanceFunction(distanceFunctionFactory.getObject(distanceFunction.getClass().getSimpleName()));
            program.setAtributeSelectionStep(createAttributeSelectionStep());
            program.setMachineLearningStep((MachineLearningStep) initialiseRandomStep(numClusters));
            createNewPreprocessingStep(program);
            program.getPreprocessingStep().setData(new Instances(data));
            population.add(program);
        }
    }

    public Population(Integer popSize, Evaluator evaluator, Instances originalData, DistanceFunction distanceFunction, Supplier<Integer> randomClusterNumber) throws Exception {
        population = new ArrayList<>();
        for (int i = 0; i < popSize; ++i) {
            Program program = new Program();
            program.setEvaluation(evaluatorFactory.getObject(evaluator.getClass().getSimpleName()));
            program.setDistanceFunction(distanceFunctionFactory.getObject(distanceFunction.getClass().getSimpleName()));
            program.setAtributeSelectionStep(createAttributeSelectionStep());
            program.setMachineLearningStep((MachineLearningStep) initialiseRandomStep(randomClusterNumber.get()));
            createNewPreprocessingStep(program);
            population.add(program);
        }
    }

    private AttributeSelectionStep createAttributeSelectionStep() {
        AttributeSelectionStep attributeSelectionStep = new AttributeSelectionStep();
//        double r=random.nextDouble();
//        if(r<0.3){
//            AttributeSelectionSubsetEvaluator attributeSelectionSubsetEvaluator=new AttributeSelectionSubsetEvaluator();
//            attributeSelectionSubsetEvaluator.setEvaluator(ObjectMaps.getEvaluatorMap().get(random.nextInt(2)).get());
//            attributeSelectionSubsetEvaluator.setSearch(ObjectMaps.getSearchMap().get(random.nextInt(2)+1).get());
//            attributeSelectionStep.setAttributeSelector(attributeSelectionSubsetEvaluator);
//        }else if(r<0.6){
//            AttributeSelectionAttributeEvaluator attributeSelectionAttributeEvaluator=new AttributeSelectionAttributeEvaluator();
//            attributeSelectionAttributeEvaluator.setEvaluator(ObjectMaps.getEvaluatorMap().get(random.nextInt(4)+2).get());
//            attributeSelectionAttributeEvaluator.setSearch(ObjectMaps.getSearchMap().get(0).get());
//            attributeSelectionStep.setAttributeSelector(attributeSelectionAttributeEvaluator);
//        }
        return attributeSelectionStep;
    }

    private void createNewPreprocessingStep(Program program) throws Exception {
        program.setPreprocessingStep(new PreprocessingStep());
        program.getPreprocessingStep().setData(program.getData());
        Filter filter = ObjectMaps.getFilterMap().get(random.nextInt(ObjectMaps.getFilterMap().keySet().size())).get();
        if (Objects.nonNull(filter)) {
            program.getPreprocessingStep().setFilter(filter);
        }
    }

    public Program getIndivid(Integer i) {
        return population.get(i);
    }

    public void addIndivid(Program program) {
        population.add(program);
    }

    public void addIndivids(List<Program> program) {
        population.addAll(program);
    }

    public Program getFittest() throws Exception {
        Program fittest = new Program();
        for (Program program : population) {
            if (program.getFitness() > fittest.getFitness()) fittest = program;
        }
        return fittest;
    }

    public Step initialiseRandomStep(Integer numClusters) {
        MachineLearningStep machineLearningStep = new MachineLearningStep();
        machineLearningStep.setClusterer(ObjectMaps.getClustererMap().get(random.nextInt(ObjectMaps.getClustererMap().size())).apply(numClusters));
        return machineLearningStep;
    }

    public Integer getSize() {
        return population.size();
    }

    public List<Program> getPopulation() {
        return population;
    }
}
