package cs.ubbcluj.model.algorithm.geneticAlgorithm.mutation;


import cs.ubbcluj.config.ObjectMaps;
import cs.ubbcluj.model.program.MachineLearningStep;
import cs.ubbcluj.model.program.Step;
import cs.ubbcluj.request.ClustererRequest;
import cs.ubbcluj.request.mutator.Request;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class ClustererMutator implements Mutator {

    private Random random;

    public ClustererMutator() {
        random = new Random();
    }

    @Override
    public void mutateClustererStepWithParams(Step machineLearningStep, Request request) {
        int size = ((ClustererRequest) request).getNumClusters() > 5 ? ObjectMaps.getClustererMap().size() - 1 : ObjectMaps.getClustererMap().size();
        ((MachineLearningStep) machineLearningStep).setClusterer(ObjectMaps.getClustererMap().get(random.nextInt(size)).apply(((ClustererRequest) request).getNumClusters()));
    }

    @Override
    public void mutateClustererStep(Step machineLearningStep) throws Exception {
        ((MachineLearningStep) machineLearningStep).setClusterer(ObjectMaps.getClustererMap().get(random.nextInt(ObjectMaps.getClustererMap().size())).apply((2)));
    }
}
