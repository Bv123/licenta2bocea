package cs.ubbcluj.model.algorithm.geneticAlgorithm;

public class GAConstants {

    public static final Integer POP_SIZE = 10;
    public static final Float CROSSOVER_RATE = 0.7F;
    public static final Float MUTATION_RATE = 0.5F;
    public static final Integer TOURNAMENT_SIZE = 6;
    public static final Integer NR_ITERATIONS = 10;
}

