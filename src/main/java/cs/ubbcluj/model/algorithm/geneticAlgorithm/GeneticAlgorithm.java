package cs.ubbcluj.model.algorithm.geneticAlgorithm;

import cs.ubbcluj.model.algorithm.Algorithm;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.mutation.ClustererMutator;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.mutation.PreprocessingMutator;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.selection.Selection;
import cs.ubbcluj.model.evaluation.Evaluator;
import cs.ubbcluj.model.program.AttributeSelectionStep;
import cs.ubbcluj.model.program.MachineLearningStep;
import cs.ubbcluj.model.program.PreprocessingStep;
import cs.ubbcluj.model.program.Program;
import cs.ubbcluj.request.ClustererRequest;
import cs.ubbcluj.utils.ProgramUtils;
import cs.ubbcluj.utils.factory.ClustererFactory;
import cs.ubbcluj.utils.factory.FilterFactory;
import weka.core.DistanceFunction;
import weka.core.Instances;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

public class GeneticAlgorithm implements Algorithm {

    private ProgramUtils programUtils = new ProgramUtils();
    private Evaluator evaluator;
    private Selection selection;
    private Instances originalData;
    private DistanceFunction distanceFunction;
    private ClustererFactory clustererFactory = new ClustererFactory();
    private FilterFactory filterFactory = new FilterFactory();
    private ClustererMutator clustererMutator = new ClustererMutator();
    private PreprocessingMutator preprocessingMutator = new PreprocessingMutator();
    private CountDownLatch countDownLatch;
    private Integer numClusters;
    private boolean findOptimumNumberOfClusters = false;

    public static ExecutorService executorService;

    private Random random = new Random();

    public GeneticAlgorithm() {
    }

    public GeneticAlgorithm(Integer numClusters) {
        this.numClusters = numClusters;
    }

    @Override
    public Program execute() throws Exception {
        Program best = new Program();
        best.setMachineLearningStep(new MachineLearningStep());
        best.setPreprocessingStep(new PreprocessingStep());
        ClustererRequest request = new ClustererRequest();
        Population pop = new Population(GAConstants.POP_SIZE, evaluator, originalData, distanceFunction, this::randomNumClusters);
        for (int i = 0; i <= GAConstants.NR_ITERATIONS; ++i) {
            countDownLatch = new CountDownLatch(pop.getSize());
            for (Program program : pop.getPopulation()) {
                if (program.executePreprocessing())
                    program.setData(new Instances(originalData));
                executorService.submit(() -> program.executeSynchronous(countDownLatch));
            }
            countDownLatch.await();
            Program pr = pop.getFittest();
            checkIfBestSoFar(best, request, pr);
            pop = evolvePopulation(pop);
        }
        displayInfoOnScreen(best);
        return best;
    }

    void displayInfoOnScreen(Program best) {
        System.out.println(best.getMachineLearningStep().getClusterer().getClass().getSimpleName());
        if (best.getPreprocessingStep().getFilter() != null) {
            System.out.println(best.getPreprocessingStep().getFilter().getClass().getSimpleName());
        } else {
            System.out.println("NULL");
        }
        System.out.println(best.getFitness());
    }

    private void checkIfBestSoFar(Program best, ClustererRequest request, Program pr) throws Exception {
        if (pr.getFitness() > best.getFitness() && pr.getMachineLearningStep().getClusters().keySet().size() > 1) {
            best.setFitness(pr.getFitness());
            best.getMachineLearningStep().setClusters(new HashMap<>(pr.getMachineLearningStep().getClusters()));
            best.getMachineLearningStep().setData(new Instances(pr.getMachineLearningStep().getData()));
            request.setNumClusters(pr.getMachineLearningStep().getClusterer().numberOfClusters());
            this.numClusters = request.getNumClusters();
            best.getMachineLearningStep().setClusterer(clustererFactory.getObjectWithParams(pr.getMachineLearningStep().getClusterer().getClass().getSimpleName(), request));
            if (pr.getPreprocessingStep().getFilter() != null) {
                best.getPreprocessingStep().setFilter(filterFactory.getObject(pr.getPreprocessingStep().getFilter().getClass().getSimpleName()));
            }
            best.setDistanceFunction(distanceFunction);
            if (pr.getAtributeSelectionStep().getAttributeSelector() != null) {
                best.setAtributeSelectionStep(new AttributeSelectionStep());
                best.getAtributeSelectionStep().setAttributeSelector(pr.getAtributeSelectionStep().getAttributeSelector().getClass().newInstance());
                best.getAtributeSelectionStep().getAttributeSelector().setEvaluator(pr.getAtributeSelectionStep().getAttributeSelector().getEvaluator().getClass().newInstance());
                best.getAtributeSelectionStep().getAttributeSelector().setSearch(pr.getAtributeSelectionStep().getAttributeSelector().getSearch().getClass().newInstance());
            }
        }
    }

    @Override
    public Instances getDataSet() {
        return originalData;
    }

    private Population evolvePopulation(Population pop) throws Exception {
        Population newPopulation = new Population();
        newPopulation.addIndivid(pop.getFittest());
        countDownLatch = new CountDownLatch(GAConstants.POP_SIZE - 1);
        for (int i = 1; i < GAConstants.POP_SIZE; ++i) {
            if (random.nextFloat() < GAConstants.CROSSOVER_RATE) {
                Program parent1 = selection.select(pop);
                Program parent2 = selection.select(pop);
                newPopulation.addIndivids(crossover(programUtils.cloneProgram(parent1), programUtils.cloneProgram(parent2)));
            }
        }
        if (newPopulation.getSize() < GAConstants.POP_SIZE)
            newPopulation.addIndivids(new Population(GAConstants.POP_SIZE - newPopulation.getSize(), evaluator, originalData, randomNumClusters(), distanceFunction).getPopulation());
        for (int i = 0; i < GAConstants.POP_SIZE; i++) {
            mutate(newPopulation.getIndivid(i));
        }
        return newPopulation;
    }

    public int randomNumClusters() {
        if (findOptimumNumberOfClusters) {
            numClusters = random.nextInt(originalData.numInstances() / 2) + 2;
        }
        return numClusters;
    }

    private void mutate(Program program) throws Exception {
        if (random.nextInt() < 0.5 || !findOptimumNumberOfClusters) {
            if (random.nextFloat() < GAConstants.MUTATION_RATE) {
                ClustererRequest clustererRequest = new ClustererRequest(randomNumClusters());
                clustererMutator.mutateClustererStepWithParams(program.getMachineLearningStep(), clustererRequest);
            }
            if (random.nextFloat() < GAConstants.MUTATION_RATE) {
                preprocessingMutator.mutateClustererStep(program.getPreprocessingStep());
                program.setExecutePreprocessing(true);
            }
        } else {
            if (program.getPreprocessingStep().getData() != null)
                program.setExecutePreprocessing(false);
            program.getMachineLearningStep().setNumClusters(randomNumClusters());
        }
    }

    private List<Program> crossover(Program p1, Program p2) throws Exception {
        List<Integer> stepsCrossover = new LinkedList<>(Arrays.asList(1, 2, 3));
        Integer operation = stepsCrossover.remove(random.nextInt(3));
        Float prob = random.nextFloat();
        stepCrossover(p1, p2, operation, prob, GAConstants.CROSSOVER_RATE);
        operation = stepsCrossover.remove(random.nextInt(stepsCrossover.size()));
        stepCrossover(p1, p2, operation, random.nextFloat(), prob);
        return Arrays.asList(p1, p2);
    }

    private void stepCrossover(Program p1, Program p2, Integer operation, Float prob, Float rate) {
        if (prob < rate) {
            if (operation == 1) {
                AttributeSelectionStep s = p2.getAtributeSelectionStep();
                p2.setAtributeSelectionStep(p1.getAtributeSelectionStep());
                p1.setAtributeSelectionStep(s);
            } else if (operation == 2) {
                MachineLearningStep s = p2.getMachineLearningStep();
                p2.setMachineLearningStep(p1.getMachineLearningStep());
                p1.setMachineLearningStep(s);
            } else if (operation == 3) {
                PreprocessingStep s = p2.getPreprocessingStep();
                p2.setPreprocessingStep(p1.getPreprocessingStep());
                p1.setPreprocessingStep(s);
                Instances data=p2.getData();
                p2.setData(p2.getData());
                p1.setData(data);
                p2.setExecutePreprocessing(true);
                p1.setExecutePreprocessing(true);
            }
        }
    }

    public Evaluator getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }

    public Selection getSelection() {
        return selection;
    }

    public void setSelection(Selection selection) {
        this.selection = selection;
    }

    public Instances getOriginalData() {
        return originalData;
    }

    public void setOriginalData(Instances originalData) {
        this.originalData = originalData;
    }

    public Integer getNumClusters() {
        return numClusters;
    }

    public void setNumClusters(Integer numClusters) {
        this.numClusters = numClusters;
    }

    public DistanceFunction getDistanceFunction() {
        return distanceFunction;
    }

    public void setDistanceFunction(DistanceFunction distanceFunction) {
        this.distanceFunction = distanceFunction;
    }

    public boolean isFindOptimumNumberOfClusters() {
        return findOptimumNumberOfClusters;
    }

    public void setFindOptimumNumberOfClusters(boolean findOptimumNumberOfClusters) {
        this.findOptimumNumberOfClusters = findOptimumNumberOfClusters;
    }
}
