package cs.ubbcluj.model.algorithm;

import cs.ubbcluj.model.evaluation.Evaluator;
import cs.ubbcluj.model.program.Program;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface Algorithm {
    Program execute() throws Exception;
    Instances getDataSet();
    Integer getNumClusters();
    Evaluator getEvaluator();
}
