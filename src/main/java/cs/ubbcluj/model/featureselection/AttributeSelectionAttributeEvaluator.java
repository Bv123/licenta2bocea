package cs.ubbcluj.model.featureselection;


import weka.attributeSelection.*;
import weka.core.Instances;

public class AttributeSelectionAttributeEvaluator extends AttributeSelectionBaseClass{

    @Override
    public int[] getAttributes(Instances data) throws Exception {
        attributeSelection.setEvaluator(evaluator);
        evaluator.buildEvaluator(data);
        attributeSelection.setSearch(search);
        attributeSelection.SelectAttributes(data);
        double[][] r = attributeSelection.rankedAttributes();
        data.setClassIndex(-1);
        int[] a = new int[r.length];
        int r1 = 0;
        for (double[] aR : r) {
            for (int j = 1; j < aR.length; ++j)
                if (aR[j] > 0.2) {
                    a[r1++] = (int) aR[0];
                }
        }
        return a;
    }

    @Override
    public <T extends  ASEvaluation> void setEvaluator(T evaluator){
        if( ! (evaluator instanceof AttributeEvaluator)){
            throw new RuntimeException("Must be attributeEvaluator");
        }
        this.evaluator = evaluator;
    }

    @Override
    public <T extends ASSearch> void setSearch(T search) {
        if(!(search instanceof Ranker))
            throw new RuntimeException("Must be ranker search");
        this.search=search;
    }

    @Override
    public ASEvaluation getEvaluator() {
        return evaluator;
    }

    @Override
    public ASSearch getSearch() {
        return search;
    }
}
