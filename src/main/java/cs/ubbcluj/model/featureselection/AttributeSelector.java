package cs.ubbcluj.model.featureselection;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.AttributeSelection;
import weka.core.Instances;

public interface AttributeSelector {
    int[] getAttributes(Instances data) throws Exception;

    <T extends ASEvaluation> void setEvaluator(T evaluator);

    <T extends ASSearch> void setSearch(T search);

    ASEvaluation getEvaluator();

    ASSearch getSearch();

    AttributeSelection getAttributeSelection();
}
