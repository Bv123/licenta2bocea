package cs.ubbcluj.model.featureselection;

import weka.attributeSelection.*;
import weka.core.Instances;

public class AttributeSelectionSubsetEvaluator extends AttributeSelectionBaseClass {

    @Override
    public int[] getAttributes(Instances data) throws Exception {
        attributeSelection.setEvaluator(evaluator);
        evaluator.buildEvaluator(data);
        attributeSelection.setSearch(search);
        attributeSelection.SelectAttributes(data);
        return attributeSelection.selectedAttributes();
    }

    @Override
    public <T extends ASEvaluation> void setEvaluator(T evaluator) {
        if (evaluator instanceof AttributeEvaluator) {
            throw new RuntimeException("Evaluator must not be AttributeEvaluator");
        }
        this.evaluator = evaluator;
    }

    @Override
    public <T extends ASSearch> void setSearch(T search) {
        if (search instanceof Ranker)
            throw new RuntimeException("Search must not be Ranker");
        this.search = search;
    }

    @Override
    public ASEvaluation getEvaluator() {
        return evaluator;
    }

    @Override
    public ASSearch getSearch() {
        return search;
    }
}
