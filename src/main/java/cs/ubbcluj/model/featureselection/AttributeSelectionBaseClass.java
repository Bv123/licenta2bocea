package cs.ubbcluj.model.featureselection;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.AttributeSelection;
import weka.core.Instances;

public abstract class AttributeSelectionBaseClass implements AttributeSelector {

    protected ASEvaluation evaluator;
    protected ASSearch search;
    protected AttributeSelection attributeSelection=new AttributeSelection();

    public abstract int[] getAttributes(Instances data) throws Exception;

    public abstract <T extends ASEvaluation> void setEvaluator(T evaluator);

    public abstract <T extends ASSearch> void setSearch(T search);

    public abstract ASEvaluation getEvaluator();

    public abstract ASSearch getSearch();

    public AttributeSelection getAttributeSelection(){
        return this.attributeSelection;
    }
}
