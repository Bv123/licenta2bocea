package cs.ubbcluj.model.similarity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class DataSetCharacteristics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer numAttr;
    private Double percentNumAttr;
    private Double percentNomAttr;
    private Double percentBinAttr;
    private Double skewness;
    private Double kurtosis;
    private Integer attributeType;
    private Double standardDeviation;
    private Double entropy;
    private double kurtosisMean;
    private double skewnessMean;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "CHAR_PROGRAM",
            joinColumns = @JoinColumn(name = "CHAR_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "PROG_ID", referencedColumnName = "ID"),
            uniqueConstraints = {
                    @UniqueConstraint(columnNames = {"CHAR_ID", "PROG_ID"})
            })
    private Set<ProgramDescr> programDescr = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumAttr() {
        return numAttr;
    }

    public void setNumAttr(Integer numAttr) {
        this.numAttr = numAttr;
    }

    public Double getPercentNumAttr() {
        return percentNumAttr;
    }

    public void setPercentNumAttr(Double percentNumAttr) {
        this.percentNumAttr = percentNumAttr;
    }

    public Double getPercentNomAttr() {
        return percentNomAttr;
    }

    public void setPercentNomAttr(Double percentNomAttr) {
        this.percentNomAttr = percentNomAttr;
    }

    public Double getPercentBinAttr() {
        return percentBinAttr;
    }

    public void setPercentBinAttr(Double percentBinAttr) {
        this.percentBinAttr = percentBinAttr;
    }

    public Double getSkewness() {
        return skewness;
    }

    public void setSkewness(Double skewness) {
        this.skewness = skewness;
    }

    public Double getKurtosis() {
        return kurtosis;
    }

    public void setKurtosis(Double kurtosis) {
        this.kurtosis = kurtosis;
    }

    public Set<ProgramDescr> getProgramDescr() {
        return programDescr;
    }

    public void setProgramDescr(Set<ProgramDescr> programDescr) {
        this.programDescr = programDescr;
    }

    public int getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(int attributeType) {
        this.attributeType = attributeType;
    }

    public Double getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(Double standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public void setAttributeType(Integer attributeType) {
        this.attributeType = attributeType;
    }

    public Double getEntropy() {
        return entropy;
    }

    public void setEntropy(Double entropy) {
        this.entropy = entropy;
    }

    public void setKurtosisMean(double kurtosisMean) {
        this.kurtosisMean = kurtosisMean;
    }

    public double getKurtosisMean() {
        return kurtosisMean;
    }

    public void setSkewnessMean(double skewnessMean) {
        this.skewnessMean = skewnessMean;
    }

    public double getSkewnessMean() {
        return skewnessMean;
    }

    @Override
    public String toString() {
        return "DataSetCharacteristics{" +
                "id=" + id +
                ", numAttr=" + numAttr +
                ", percentNumAttr=" + percentNumAttr +
                ", percentNomAttr=" + percentNomAttr +
                ", percentBinAttr=" + percentBinAttr +
                ", skewness=" + skewness +
                ", kurtosis=" + kurtosis +
                ", attributeType=" + attributeType +
                ", standardDeviation=" + standardDeviation +
                ", entropy=" + entropy +
                ", kurtosisMean=" + kurtosisMean +
                ", skewnessMean=" + skewnessMean +
                '}';
    }
}
