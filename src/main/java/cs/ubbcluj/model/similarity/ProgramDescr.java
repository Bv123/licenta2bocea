package cs.ubbcluj.model.similarity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ProgramDescr {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String algorithm;
    private String preprocessing;
    private String attrSelection;
    private String distanceFunction;
    private Integer numClusters;

    @ElementCollection
    private List<String> parameters;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getPreprocessing() {
        return preprocessing;
    }

    public void setPreprocessing(String preprocessing) {
        this.preprocessing = preprocessing;
    }

    public String getAttrSelection() {
        return attrSelection;
    }

    public void setAttrSelection(String attrSelection) {
        this.attrSelection = attrSelection;
    }

    public String getDistanceFunction() {
        return distanceFunction;
    }

    public void setDistanceFunction(String distanceFunction) {
        this.distanceFunction = distanceFunction;
    }

    public Integer getNumClusters() {
        return numClusters;
    }

    public void setNumClusters(Integer numClusters) {
        this.numClusters = numClusters;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public void setParameters(List<String> parameters) {
        this.parameters = parameters;
    }
}
