package cs.ubbcluj.model.program;

import weka.clusterers.Clusterer;
import weka.clusterers.FarthestFirst;
import weka.clusterers.NumberOfClustersRequestable;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MachineLearningStep extends AbstractStep {

    private Clusterer clusterer;
    private Map<Integer, List<Instance>> clusters;
    private Map<Instance, Integer> instanceCluster;

    public MachineLearningStep() {
    }

    private void createClusterMap(Instances data) throws Exception {
        clusters = new HashMap<>();
        instanceCluster = new HashMap<>();
        for (Instance i : data) {
            Integer mapkey = clusterer.clusterInstance(i);
            if (clusters.containsKey(mapkey)) {
                instanceCluster.put(i, mapkey);
                clusters.get(mapkey).add(i);
            } else {
                instanceCluster.put(i, mapkey);
                List<Instance> clusterList = new ArrayList<>();
                clusterList.add(i);
                clusters.put(mapkey, clusterList);
            }
        }
    }

    public void setNumClusters(int numClusters) throws Exception {
        if (clusterer instanceof NumberOfClustersRequestable)
            ((NumberOfClustersRequestable) clusterer).setNumClusters(numClusters);
        else if (clusterer instanceof FarthestFirst) {
            ((FarthestFirst) clusterer).setNumClusters(numClusters);
        } else
            throw new UnsupportedOperationException("cluster does not support set number of clusters");
    }

    public Clusterer getClusterer() {
        return clusterer;
    }

    public void setClusterer(Clusterer clusterer) {
        this.clusterer = clusterer;
    }

    public Map<Integer, List<Instance>> getClusters() {
        return clusters;
    }

    public void setClusters(Map<Integer, List<Instance>> clusters) {
        this.clusters = clusters;
    }

    public Map<Instance, Integer> getInstanceCluster() {
        return instanceCluster;
    }

    public void setInstanceCluster(Map<Instance, Integer> instanceCluster) {
        this.instanceCluster = instanceCluster;
    }

    @Override
    public void execute() throws Exception {
        clusterer.buildClusterer(data);
        createClusterMap(data);
    }

    @Override
    public String toString() {
        return "MachineLearningStep{" +
                "clusterer=" + clusterer +
                '}';
    }
}
