package cs.ubbcluj.model.program;

import cs.ubbcluj.model.evaluation.Evaluator;
import weka.core.DistanceFunction;
import weka.core.Instances;

import java.util.concurrent.CountDownLatch;

public class Program {

    private PreprocessingStep preprocessingStep;
    private AttributeSelectionStep atributeSelectionStep;
    private MachineLearningStep machineLearningStep;
    private Evaluator evaluation;
    private Double fitness = -2D;
    private DistanceFunction distanceFunction;
    private Instances data;
    private boolean executePreprocessing = true;

    public Double getFitness() {
        return fitness;
    }

    public void setFitness(Double fitness) {
        this.fitness = fitness;
    }

    public PreprocessingStep getPreprocessingStep() {
        return preprocessingStep;
    }

    public void setPreprocessingStep(PreprocessingStep preprocessingStep) {
        this.preprocessingStep = preprocessingStep;
    }

    public AttributeSelectionStep getAtributeSelectionStep() {
        return atributeSelectionStep;
    }

    public void setAtributeSelectionStep(AttributeSelectionStep atributeSelectionStep) {
        this.atributeSelectionStep = atributeSelectionStep;
    }

    public MachineLearningStep getMachineLearningStep() {
        return machineLearningStep;
    }

    public void setMachineLearningStep(MachineLearningStep machineLearningStep) {
        this.machineLearningStep = machineLearningStep;
    }

    public Evaluator getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluator evaluation) {
        this.evaluation = evaluation;
    }

    public Double executeSynchronous(CountDownLatch countDownLatch) {
        try {
            return executeProgram();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            countDownLatch.countDown();
        }
        return 0D;
    }

    private Double executeProgram() throws Exception {
        if (executePreprocessing || preprocessingStep.getData()==null) {
            preprocessingStep.setData(data);
            preprocessingStep.execute();
        }
        machineLearningStep.setData(preprocessingStep.getData());
        machineLearningStep.execute();
        evaluation.setCluster(machineLearningStep.getClusters());
        evaluation.setInstanceCluster(machineLearningStep.getInstanceCluster());
        distanceFunction.setInstances(machineLearningStep.getData());
        this.evaluation.setCluster(machineLearningStep.getClusters());
        this.evaluation.setInstanceCluster(machineLearningStep.getInstanceCluster());
        this.fitness = evaluation.getCoefficient(machineLearningStep.getData(), distanceFunction);
        return this.fitness;
    }

    public Double execute() {
        try {
            return executeProgram();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0D;
    }

    public DistanceFunction getDistanceFunction() {
        return distanceFunction;
    }

    public void setDistanceFunction(DistanceFunction distanceFunction) {
        this.distanceFunction = distanceFunction;
    }

    public Instances getData() {
        return data;
    }

    public void setData(Instances data) {
        this.data = data;
    }

    public boolean executePreprocessing() {
        return executePreprocessing;
    }

    public void setExecutePreprocessing(boolean executePreprocessing) {
        this.executePreprocessing = executePreprocessing;
    }

    @Override
    public String toString() {
        return "Program{" +
                "preprocessingStep=" + preprocessingStep +
                ", atributeSelectionStep=" + atributeSelectionStep +
                ", machineLearningStep=" + machineLearningStep +
                ", evaluation=" + evaluation +
                ", fitness=" + fitness +
                '}';
    }
}
