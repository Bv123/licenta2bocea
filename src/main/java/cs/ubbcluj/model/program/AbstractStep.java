package cs.ubbcluj.model.program;

import weka.core.Instances;

public abstract class AbstractStep implements Step {

    protected Instances data;

    @Override
    public abstract void execute() throws Exception;

    @Override
    public void setData(Instances data) {
        this.data=data;
    }

    @Override
    public Instances getData() {
        return data;
    }
}
