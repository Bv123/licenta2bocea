package cs.ubbcluj.model.program;

import cs.ubbcluj.model.featureselection.AttributeSelector;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.util.Objects;

public class AttributeSelectionStep extends AbstractStep {

    private AttributeSelector attributeSelector;

    @Override
    public void execute() throws Exception {
        if (Objects.nonNull(attributeSelector)) {
            Remove filter = new Remove();
            filter.setInputFormat(data);
            filter.setInvertSelection(true);
            filter.setAttributeIndicesArray(attributeSelector.getAttributes(data));
            this.data = Filter.useFilter(data, filter);
        }
    }

    public AttributeSelector getAttributeSelector() {
        return attributeSelector;
    }

    public void setAttributeSelector(AttributeSelector attributeSelector) {
        this.attributeSelector = attributeSelector;
    }
}
