package cs.ubbcluj.model.program;

import weka.filters.Filter;

import java.util.Objects;

public class PreprocessingStep extends AbstractStep {

    private Filter filter;

    @Override
    public void execute() throws Exception {
        if (Objects.nonNull(filter)) {
            filter.setInputFormat(this.data);
            this.data = Filter.useFilter(this.data, filter);
        }
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public Filter getFilter() {
        return filter;
    }
}
