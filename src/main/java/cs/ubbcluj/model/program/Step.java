package cs.ubbcluj.model.program;

import weka.core.Instances;

public interface Step {
    void execute() throws Exception;
    void setData(Instances data);
    Instances getData();
}
