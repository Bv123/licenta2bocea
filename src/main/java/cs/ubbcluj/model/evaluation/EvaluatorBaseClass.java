package cs.ubbcluj.model.evaluation;

import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

public abstract class EvaluatorBaseClass implements Evaluator {

    protected Map<Integer, List<Instance>> cluster;
    protected Map<Instance, Integer> instanceCluster;

    @Override
    public abstract Double getCoefficient(Instances data,DistanceFunction distanceFunction);

    @Override
    public Map<Integer, List<Instance>> getCluster() {
        return cluster;
    }

    @Override
    public void setCluster(Map<Integer, List<Instance>> cluster) {
        this.cluster = cluster;
    }

    @Override
    public Map<Instance, Integer> getInstanceCluster() {
        return instanceCluster;
    }

    @Override
    public void setInstanceCluster(Map<Instance, Integer> instanceCluster) {
        this.instanceCluster = instanceCluster;
    }
}
