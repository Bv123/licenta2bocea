package cs.ubbcluj.model.evaluation;


import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;

import java.util.List;
import java.util.Map;

public interface Evaluator {
    Double getCoefficient(Instances data,DistanceFunction distanceFunction);

    Map<Integer, List<Instance>> getCluster();

    void setCluster(Map<Integer, List<Instance>> cluster);

    Map<Instance, Integer> getInstanceCluster();

    void setInstanceCluster(Map<Instance, Integer> instanceCluster);
}
