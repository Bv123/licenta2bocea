package cs.ubbcluj.model.evaluation;


import weka.core.DistanceFunction;
import weka.core.Instance;
import weka.core.Instances;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SilhouetteEvaluator extends EvaluatorBaseClass {

    @Override
    public Double getCoefficient(Instances data, DistanceFunction distanceFunction) {
        Double ai;
        Double si = 0D;
        for (Instance i : data) {
            ai = computeSilhouette(distanceFunction, i, cluster.get(instanceCluster.get(i)));
            Double bi = Double.MAX_VALUE;
            for (Map.Entry<Integer, List<Instance>> l : cluster.entrySet()) {
                if (!l.getKey().equals(instanceCluster.get(i)) && Objects.nonNull(l.getValue())) {
                    Double sum = computeSilhouette(distanceFunction, i, l.getValue());
                    if (sum < bi) bi = sum;
                }
            }
            si += (bi - ai) / Math.max(ai, bi);
        }
        return si / data.size();
    }

    private Double computeSilhouette(DistanceFunction dist, Instance i, List<Instance> cluster) {
        Double s = 0D;
        for (Instance j : cluster) {
            s += dist.distance(i, j);
        }
        return s / cluster.size();
    }
}
