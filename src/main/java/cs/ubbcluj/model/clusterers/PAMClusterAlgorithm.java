package cs.ubbcluj.model.clusterers;


import weka.clusterers.Clusterer;
import weka.clusterers.NumberOfClustersRequestable;
import weka.core.*;

import java.util.*;

public class PAMClusterAlgorithm implements Clusterer,NumberOfClustersRequestable {

    private int numClusters = 2;
    private int maxIterations = 100;
    private List<Instance> medoids = new ArrayList<>();
    private List<Instance> nonMedoids = new ArrayList<>();
    private Map<Instance, Double> distanceToMedoid = new HashMap<>();
    private Map<Instance, Instance> instanceToMedoid = new HashMap<>();
    private DistanceFunction distanceFunction = new EuclideanDistance();
    private Map<Instance, Integer> clusters = new HashMap<>();

    @Override
    public void buildClusterer(Instances data) throws Exception {
        Collections.shuffle(data);
        distanceFunction.setInstances(data);
        processData(data);
        Double currentCost=Double.MAX_VALUE;
        Double totalCost = Double.POSITIVE_INFINITY;
        while (currentCost < totalCost && maxIterations > 0) {
            --maxIterations;
            totalCost=currentCost;
            Double min=Double.POSITIVE_INFINITY;
            for (int i = 0; i < medoids.size(); ++i) {
                for (int j = 0; j < nonMedoids.size(); ++j) {
                    swapElements(i, j);
                    currentCost = computeCostOfConfiguration(medoids.get(i), nonMedoids.get(j), totalCost, i, j);
                    if (currentCost<min )
                        min=currentCost;
                }
            }
            currentCost=min;
        }
        createClusters();
    }

    private void createClusters() {
        int clusterNumber = 0;
        for (Instance i : medoids) {
            clusters.put(i, clusterNumber++);
        }
    }


    //TODO FIX DESIGN OF THIS METHOD
    private Double computeCostOfConfiguration(Instance newMedoid, Instance oldMedoid,
                                              double previousCost, int i, int j) {
        Map<Instance, Double> temporaryDistanceMap = new HashMap<>();
        Map<Instance, Instance> temporaryMedoidMap = new HashMap<>();

        final double[] totalSum = {0};
        nonMedoids.forEach(instance -> {
            if (instanceToMedoid.get(instance) == oldMedoid) {
                Double minDistance = Double.MAX_VALUE;
                Instance selectedMedoid = null;
                for (Instance medoid : medoids) {
                    Double distance = distanceFunction.distance(instance, medoid);
                    if (distance < minDistance) {
                        selectedMedoid = medoid;
                        minDistance = distance;
                    }
                }
                totalSum[0] += minDistance;
                temporaryDistanceMap.put(instance, minDistance);
                temporaryMedoidMap.put(instance, selectedMedoid);
            } else {
                Double newDistance = distanceFunction.distance(instance, newMedoid);
                Double currentDistance = distanceToMedoid.get(instance);
                if (newDistance < currentDistance) {
                    temporaryDistanceMap.put(instance, newDistance);
                    temporaryMedoidMap.put(instance, newMedoid);
                    totalSum[0] += newDistance;
                } else {
                    totalSum[0] += currentDistance;
                }
            }
        });
        if (totalSum[0] > previousCost) {
            swapElements(i, j);
            Double min=Double.POSITIVE_INFINITY;
            Instance selectedMedoid=null;
            for(Instance medoid:medoids){
                Double currentDist=distanceFunction.distance(nonMedoids.get(j),medoid);
                if (currentDist<min){
                    min=currentDist;
                    selectedMedoid=medoid;
                }
            }
            instanceToMedoid.put(nonMedoids.get(j),selectedMedoid);
            distanceToMedoid.put(nonMedoids.get(j),min);
        } else {
            for (Instance instance : temporaryDistanceMap.keySet()) {
                distanceToMedoid.put(instance, temporaryDistanceMap.get(instance));
                instanceToMedoid.put(instance, temporaryMedoidMap.get(instance));
            }
        }
        return totalSum[0];
    }

    private void swapElements(int i, int j) {
        Instance current = nonMedoids.get(j);
        nonMedoids.set(j, medoids.get(i));
        medoids.set(i, current);
        instanceToMedoid.put(nonMedoids.get(j), medoids.get(i));
        instanceToMedoid.put(medoids.get(i), medoids.get(i));
        distanceToMedoid.put(nonMedoids.get(j),distanceFunction.distance(nonMedoids.get(j),medoids.get(i)));
        distanceToMedoid.put(medoids.get(i),0D);
    }

    private void processData(Instances data) {
        for (int i = 0; i < numClusters; ++i) {
            medoids.add(data.get(i));
            instanceToMedoid.put(data.get(i), data.get(i));
            distanceToMedoid.put(data.get(i), 0D);
        }
        for (int i = numClusters; i < data.size(); ++i) {
            Instance current = data.get(i);
            //TODO refactor to java 8
            Double minDistance = Double.MAX_VALUE;
            Instance selectedMedoid = null;
            for (Instance medoid : medoids) {
                Double distance = distanceFunction.distance(current, medoid);
                if (distance < minDistance) {
                    selectedMedoid = medoid;
                    minDistance = distance;
                }
            }
            distanceToMedoid.put(current, minDistance);
            instanceToMedoid.put(current, selectedMedoid);
            nonMedoids.add(current);
        }
    }

    @Override
    public int clusterInstance(Instance instance) throws Exception {
        if (instanceToMedoid.containsKey(instance)) {
            return clusters.get(instanceToMedoid.get(instance));
        } else {
            double minDist = Double.MAX_VALUE;
            Instance medoid = null;
            for (Instance i : medoids) {
                Double distanceToMedoid = distanceFunction.distance(i, instance);
                if (distanceToMedoid < minDist) {
                    minDist = distanceToMedoid;
                    medoid = i;
                }
            }
            return clusters.get(medoid);
        }
    }

    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
        return new double[0];
    }

    @Override
    public int numberOfClusters() throws Exception {
        return numClusters;
    }

    @Override
    public Capabilities getCapabilities() {
        return null;
    }

    public List<Instance> getMedoids() {
        return medoids;
    }

    public void setMedoids(List<Instance> medoids) {
        this.medoids = medoids;
    }

    public List<Instance> getNonMedoids() {
        return nonMedoids;
    }

    public void setNonMedoids(List<Instance> nonMedoids) {
        this.nonMedoids = nonMedoids;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    public DistanceFunction getDistanceFunction() {
        return distanceFunction;
    }

    public void setDistanceFunction(DistanceFunction distanceFunction) {
        this.distanceFunction = distanceFunction;
    }

    @Override
    public void setNumClusters(int numClusters) throws Exception {
        this.numClusters=numClusters;
    }
}
