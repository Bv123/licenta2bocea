package cs.ubbcluj.model.clusterers;

import weka.clusterers.Clusterer;
import weka.core.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DBScanAlgorithm implements Clusterer {

    private int numClusters;
    private DistanceFunction distanceFunction;
    private double epsilon;
    private int minP;
    private Instances data;
    private Map<Integer, List<Instance>> clusters = new HashMap<>();
    private Map<Instance, Integer> instanceCluster = new HashMap<>();

    public DBScanAlgorithm(int numClusters, DistanceFunction distanceFunction, double epsilon, int minP) {
        this.numClusters = numClusters;
        this.distanceFunction = distanceFunction;
        this.epsilon = epsilon;
        this.minP = minP;
    }

    public DBScanAlgorithm() {
        this.epsilon = 0.57;
    }

    private List<Instance> createNeighbourList(Instance i) {
        return data.stream().filter(x -> x != i && distanceFunction.distance(i, x) <= epsilon).collect(Collectors.toList());
    }

    private void union(List<Instance> a, List<Instance> b) {
        b.stream().filter(x -> !a.contains(x)).forEach(a::add);
    }

    @Override
    public void buildClusterer(Instances data) throws Exception {
        final int[] clusterCount = {0};
        distanceFunction=new EuclideanDistance();
        distanceFunction.setInstances(data);
        this.data = data;
        minP = data.numAttributes()+1;
        Map<Instance, Boolean> typeMap = new HashMap<>();
        data.forEach(instance -> {
            if (typeMap.get(instance) == null) {
                List<Instance> nList = createNeighbourList(instance);
                if (nList.size() >= minP) {
                    List<Instance> expandedCluster = expandCluster(instance, nList, typeMap);
                    clusters.put(clusterCount[0]++, expandedCluster);
                    expandedCluster.forEach(inst -> instanceCluster.put(inst, clusterCount[0]));
                } else {
                    typeMap.put(instance, Boolean.TRUE);
                }
            }
        });
    }

    private List<Instance> expandCluster(Instance instance, List<Instance> nList, Map<Instance, Boolean> typeMap) {
        List<Instance> cluster = new ArrayList<>();
        cluster.add(instance);
        typeMap.put(instance, false);
        List<Instance> nListCopy = new ArrayList<>(nList);
        for (int i=0;i < nListCopy.size();++i) {
            Instance inst = nListCopy.get(i);
            Boolean type = typeMap.get(inst);
            if (type == null) {
                List<Instance> epsilonNeighbour = this.createNeighbourList(inst);
                if (epsilonNeighbour.size() >= minP) {
                    union(nListCopy, epsilonNeighbour);
                }
            }
            if (type == null || type) {
                typeMap.put(inst, false);
                cluster.add(inst);
            }
        }
        return cluster;
    }

    @Override
    public int clusterInstance(Instance instance) throws Exception {
        if (!instanceCluster.containsKey(instance)) {
            double minDist = Double.MAX_VALUE;
            int clusterIndex=-1;
            for (Map.Entry<Integer, List<Instance>> cluster : clusters.entrySet()) {
                Double sum=cluster.getValue().stream().mapToDouble(inst->distanceFunction.distance(inst,instance)).sum();
                if (sum/cluster.getValue().size()<minDist){
                    minDist=sum/cluster.getValue().size();
                    clusterIndex=cluster.getKey();
                }
            }
            return clusterIndex;
        }
        return instanceCluster.get(instance);
    }

    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
        return new double[0];
    }

    @Override
    public int numberOfClusters() throws Exception {
        return numClusters;
    }

    @Override
    public Capabilities getCapabilities() {
        return null;
    }

    public void setNumClusters(int numClusters) {
        this.numClusters = numClusters;
    }
}
