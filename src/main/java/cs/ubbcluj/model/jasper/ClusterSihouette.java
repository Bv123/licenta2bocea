package cs.ubbcluj.model.jasper;


import java.util.List;

public class ClusterSihouette {
    private Double silhouetteCoefficient;
    private List<ClusterInstances> clusterInstances;

    public Double getSilhouetteCoefficient() {
        return silhouetteCoefficient;
    }

    public void setSilhouetteCoefficient(Double silhouetteCoefficient) {
        this.silhouetteCoefficient = silhouetteCoefficient;
    }

    public List<ClusterInstances> getClusterInstances() {
        return clusterInstances;
    }

    public void setClusterInstances(List<ClusterInstances> clusterInstances) {
        this.clusterInstances = clusterInstances;
    }
}
