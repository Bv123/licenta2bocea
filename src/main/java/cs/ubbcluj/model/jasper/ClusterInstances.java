package cs.ubbcluj.model.jasper;

import weka.core.Instance;

import java.util.List;

public class ClusterInstances {
    private Integer integer;
    private List<Instance> instances;

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public List<Instance> getInstances() {
        return instances;
    }

    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }
}
