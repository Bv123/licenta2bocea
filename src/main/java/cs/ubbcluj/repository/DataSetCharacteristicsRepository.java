package cs.ubbcluj.repository;

import cs.ubbcluj.model.similarity.DataSetCharacteristics;
import cs.ubbcluj.model.similarity.ProgramDescr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataSetCharacteristicsRepository extends JpaRepository<DataSetCharacteristics, Long> {

    @Query(value = "select (d.kurtosis * :kurtosis + d.num_attr * :numAttr + d.percent_bin_attr * :percentBinAttr  + d.percent_nom_attr * :percentNomAttr +" +
            "d.percent_num_attr * :percentNumAttr +d.skewness* :skewness + d.attribute_type * :attributeType + d.standard_deviation* :standardDeviation+ d.entropy*:entropy )/" +
            "(sqrt(d.kurtosis * d.kurtosis + d.num_attr * d.num_attr + d.percent_bin_attr*d.percent_bin_attr + d.percent_nom_attr*d.percent_nom_attr +" +
            "d.percent_num_attr*d.percent_num_attr +d.skewness*d.skewness + d.attribute_type*d.attribute_type + d.standard_deviation*d.standard_deviation+d.entropy*d.entropy)*" +
            "sqrt(:kurtosis * :kurtosis + :numAttr * :numAttr + :percentBinAttr*:percentBinAttr + :percentNomAttr*:percentNomAttr+" +
            ":percentNumAttr*:percentNumAttr +:skewness*:skewness + :attributeType*:attributeType + :standardDeviation*:standardDeviation +:entropy*:entropy)) from data_set_characteristics d ", nativeQuery = true)
    List<Double> select(@Param("entropy") Double entropy, @Param("standardDeviation") Double standardDeviation, @Param("numAttr") Integer numAttr, @Param("attributeType") Integer attributeType, @Param("percentBinAttr") double percentBinAttr, @Param("percentNomAttr") double percentNomAttr, @Param("percentNumAttr") double percentNumAttr, @Param("skewness") double skewness, @Param("kurtosis") double kurtosis);

    @Query(value = "select * from data_set_characteristics d where  d.kurtosis =:kurtosis and d.num_attr =:numAttr and d.percent_bin_attr =:percentBinAttr  and d.percent_nom_attr =:percentNomAttr and " +
            "d.percent_num_attr =:percentNumAttr and d.skewness=:skewness and d.skewness_mean=:skewnessMean   and d.kurtosis_mean=:kurtosisMean and d.attribute_type = :attributeType and d.standard_deviation=:standardDeviation and d.entropy=:entropy limit 1", nativeQuery = true)
    DataSetCharacteristics findByAll(@Param("entropy") Double entropy, @Param("standardDeviation") Double standardDeviation, @Param("numAttr") Integer numAttr, @Param("attributeType") Integer attributeType, @Param("percentBinAttr") double percentBinAttr, @Param("percentNomAttr") double percentNomAttr, @Param("percentNumAttr") double percentNumAttr, @Param("skewnessMean") double skewnessMean, @Param("kurtosisMean") double kurtosisMean ,@Param("skewness") double skewness, @Param("kurtosis") double kurtosis);
}
