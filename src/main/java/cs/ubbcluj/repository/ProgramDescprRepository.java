package cs.ubbcluj.repository;

import cs.ubbcluj.model.similarity.ProgramDescr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramDescprRepository extends JpaRepository<ProgramDescr, Long> {
    @Query("Select p from ProgramDescr p where p.algorithm= :algorithm and p.attrSelection IS NULL and p.preprocessing= :preprocessing and p.distanceFunction =:distanceFunction and p.numClusters= :numClusters")
    ProgramDescr checkIfProgramDescExists(@Param("algorithm") String algorithm, @Param("preprocessing") String preprocessing, @Param("distanceFunction") String distanceFunction, @Param("numClusters") Integer numClusters);

    @Query(value = "select p.id,p.algorithm,p.preprocessing,p.attr_selection,p.distance_function,p.num_clusters from data_set_characteristics d inner join CHAR_PROGRAM c on d.id=c.CHAR_ID " +
            "inner join program_descr p on p.id = c.PROG_ID " +
            "where d.attribute_type =:attributeType and (d.kurtosis * :kurtosis + d.num_attr * :numAttr + d.percent_bin_attr * :percentBinAttr  + d.percent_nom_attr * :percentNomAttr +" +
            "d.percent_num_attr * :percentNumAttr +d.skewness* :skewness + d.standard_deviation* :standardDeviation+ d.entropy*:entropy )/" +
            "(sqrt(d.kurtosis * d.kurtosis + d.num_attr * d.num_attr + d.percent_bin_attr*d.percent_bin_attr + d.percent_nom_attr*d.percent_nom_attr +" +
            "d.percent_num_attr*d.percent_num_attr +d.skewness*d.skewness + d.standard_deviation*d.standard_deviation+d.entropy*d.entropy)*" +
            "sqrt(:kurtosis * :kurtosis + :numAttr * :numAttr + :percentBinAttr*:percentBinAttr + :percentNomAttr*:percentNomAttr+" +
            ":percentNumAttr*:percentNumAttr +:skewness*:skewness + :standardDeviation*:standardDeviation +:entropy*:entropy)) > 0.9 and p.num_clusters=:numClusters limit 10 ", nativeQuery = true)
    List<ProgramDescr> matchesDataSetCharacteristics(@Param("numClusters") Integer numClusters, @Param("entropy") Double entropy, @Param("standardDeviation") Double standardDeviation, @Param("numAttr") Integer numAttr, @Param("attributeType") Integer attributeType, @Param("percentBinAttr") double percentBinAttr, @Param("percentNomAttr") double percentNomAttr, @Param("percentNumAttr") double percentNumAttr, @Param("skewness") double skewness, @Param("kurtosis") double kurtosis);

}
