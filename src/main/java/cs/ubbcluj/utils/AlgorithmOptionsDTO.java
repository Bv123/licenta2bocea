package cs.ubbcluj.utils;


import cs.ubbcluj.utils.enums.ClustererEnum;
import cs.ubbcluj.utils.enums.DistanceFunctionEnum;
import cs.ubbcluj.utils.enums.EvaluatorEnum;
import cs.ubbcluj.utils.enums.FilterEnum;

public class AlgorithmOptionsDTO {
    private FilterEnum[] filters;
    private ClustererEnum[] clusterers;
    private EvaluatorEnum[] evaluators;
    private DistanceFunctionEnum[] distanceFunctions;

    public FilterEnum[] getFilters() {
        return filters;
    }

    public void setFilters(FilterEnum[] filterEnums) {
        this.filters = filterEnums;
    }

    public ClustererEnum[] getClusterers() {
        return clusterers;
    }

    public void setClusterers(ClustererEnum[] clusterers) {
        this.clusterers = clusterers;
    }

    public EvaluatorEnum[] getEvaluators() {
        return evaluators;
    }

    public void setEvaluators(EvaluatorEnum[] evaluators) {
        this.evaluators = evaluators;
    }

    public DistanceFunctionEnum[] getDistanceFunctions() {
        return distanceFunctions;
    }

    public void setDistanceFunctions(DistanceFunctionEnum[] distanceFunctions) {
        this.distanceFunctions = distanceFunctions;
    }
}
