package cs.ubbcluj.utils.factory;


import cs.ubbcluj.request.factory.FactoryRequest;
import org.springframework.stereotype.Component;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.*;
import weka.filters.unsupervised.instance.Resample;

@Component
public class FilterFactory implements ObjectFactory<Filter> {
    @Override
    public Filter getObjectWithParams(String className, FactoryRequest factoryRequest) throws Exception {
        return getObject(className);
    }

    @Override
    public Filter getObject(String className) throws Exception {
        if (className == null) return null;
        switch (className) {
            case "Standardize":
                return new Standardize();
            case "Discretize":
                return new Discretize();
            case "Normalize":
                return new Normalize();
            case "Resample":
                return new Resample();
            case "PrincipalComponents":
                return new PrincipalComponents();
            case "ReplaceMissingValues":
                return new ReplaceMissingValues();
            case "NominalToBinary":
                return new NominalToBinary();
            case "null":
                return null;
            case "NULL":
                return null;
        }
        throw new RuntimeException("Unsupported FIlter");
    }
}
