package cs.ubbcluj.utils.factory;

import cs.ubbcluj.model.evaluation.Evaluator;
import cs.ubbcluj.model.evaluation.SilhouetteEvaluator;
import cs.ubbcluj.request.factory.FactoryRequest;
import org.springframework.stereotype.Component;

@Component
public class EvaluatorFactory implements ObjectFactory<Evaluator> {

    @Override
    public Evaluator getObjectWithParams(String className, FactoryRequest factoryRequest) throws Exception {
        return getObject(className);
    }

    @Override
    public Evaluator getObject(String className) throws Exception {
        switch (className) {
            case "SilhouetteEvaluator":
                return new SilhouetteEvaluator();
        }
        throw new RuntimeException("Unsupported Evaluator");
    }
}
