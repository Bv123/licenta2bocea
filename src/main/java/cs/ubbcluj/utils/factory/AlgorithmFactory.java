package cs.ubbcluj.utils.factory;

import cs.ubbcluj.model.algorithm.Algorithm;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.GeneticAlgorithm;
import cs.ubbcluj.request.ClustererRequest;
import cs.ubbcluj.request.factory.FactoryRequest;
import org.springframework.stereotype.Component;

@Component
public class AlgorithmFactory implements ObjectFactory<Algorithm> {
    @Override
    public Algorithm getObjectWithParams(String algorithm, FactoryRequest request) throws Exception {
        ClustererRequest clustererRequest = (ClustererRequest) request;
        switch (algorithm) {
            case "GeneticAlgorithm": {
                return new GeneticAlgorithm(clustererRequest.getNumClusters());
            }
        }
        throw new RuntimeException("No such algorithm");
    }

    @Override
    public Algorithm getObject(String algorithm) throws Exception {
        ClustererRequest clustererRequest = new ClustererRequest(2);
        return getObjectWithParams(algorithm, clustererRequest);
    }
}
