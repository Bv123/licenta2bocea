package cs.ubbcluj.utils.factory;


import cs.ubbcluj.model.clusterers.DBScanAlgorithm;
import cs.ubbcluj.model.clusterers.PAMClusterAlgorithm;
import cs.ubbcluj.request.ClustererRequest;
import cs.ubbcluj.request.factory.FactoryRequest;
import org.springframework.stereotype.Component;
import weka.clusterers.*;

@Component
public class ClustererFactory implements ObjectFactory<Clusterer> {

    public Clusterer getObjectWithParams(String className, FactoryRequest request) throws Exception {
        ClustererRequest clustererRequest = (ClustererRequest) request;
        return getClusterer(className, clustererRequest.getNumClusters());
    }

    private Clusterer getClusterer(String className, Integer numClusters) throws Exception {
        switch (className) {
            case "SimpleKMeans": {
                SimpleKMeans simpleKMeans = new SimpleKMeans();
                simpleKMeans.setNumClusters(numClusters);
                return simpleKMeans;
            }
            case "DBScanAlgorithm": {
                return new DBScanAlgorithm();
            }
            case "PAMClusterAlgorithm": {
                PAMClusterAlgorithm clusterAlgorithm = new PAMClusterAlgorithm();
                clusterAlgorithm.setNumClusters(numClusters);
                return clusterAlgorithm;
            }
            case "Canopy": {
                Canopy canopy = new Canopy();
                canopy.setNumClusters(numClusters);
                return canopy;
            }
            case "FarthestFirst": {
                FarthestFirst farthestFirst = new FarthestFirst();
                farthestFirst.setNumClusters(numClusters);
                return farthestFirst;
            }
            case "MakeDensityBasedClusterer": {
                MakeDensityBasedClusterer makeDensityBasedClusterer = new MakeDensityBasedClusterer();
                makeDensityBasedClusterer.setNumClusters(numClusters);
                return makeDensityBasedClusterer;
            }
            case "EM": {
                EM em = new EM();
                em.setNumClusters(numClusters);
                return em;
            }
        }
        throw new RuntimeException("Unsupported Clusterer");
    }

    @Override
    public Clusterer getObject(String className) throws Exception {
        return getClusterer(className, 2);
    }
}
