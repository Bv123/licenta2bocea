package cs.ubbcluj.utils.factory;

import cs.ubbcluj.model.algorithm.geneticAlgorithm.selection.RouletteWheelSelection;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.selection.Selection;
import cs.ubbcluj.model.algorithm.geneticAlgorithm.selection.TournamentSelection;
import cs.ubbcluj.request.factory.FactoryRequest;
import org.springframework.stereotype.Component;

@Component
public class SelectionFactory implements ObjectFactory<Selection> {
    @Override
    public Selection getObjectWithParams(String selection, FactoryRequest request) throws Exception {
        return getObject(selection);
    }

    @Override
    public Selection getObject(String selection) throws Exception {
        switch (selection) {
            case "TournamentSelection":return new TournamentSelection();
            case "RoulleteWheelSelection":return new RouletteWheelSelection();
        }
        throw new RuntimeException("Unsupported Selection type");
    }
}
