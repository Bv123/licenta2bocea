package cs.ubbcluj.utils.factory;

import cs.ubbcluj.request.factory.FactoryRequest;

public interface ObjectFactory<T> {
    T getObjectWithParams(String className, FactoryRequest request) throws Exception;

    T getObject(String className) throws Exception;
}
