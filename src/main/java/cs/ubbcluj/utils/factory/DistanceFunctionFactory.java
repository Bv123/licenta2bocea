package cs.ubbcluj.utils.factory;

import cs.ubbcluj.request.factory.FactoryRequest;
import org.springframework.stereotype.Component;
import weka.core.*;
@Component
public class DistanceFunctionFactory implements ObjectFactory<DistanceFunction> {

    @Override
    public DistanceFunction getObjectWithParams(String className, FactoryRequest request) throws Exception {
        return getObject(className);
    }

    @Override
    public DistanceFunction getObject(String className) throws Exception {
        switch (className) {
            case "ChebyshevDistance":
                return new ChebyshevDistance();
            case "EuclideanDistance":
                return new EuclideanDistance();
            case "ManhattanDistance":
                return new ManhattanDistance();
            case "MinkowskiDistance":
                return new MinkowskiDistance();
        }
        throw new RuntimeException("Unsupported Distance Function");
    }
}
