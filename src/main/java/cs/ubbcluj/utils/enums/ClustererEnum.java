package cs.ubbcluj.utils.enums;

public enum ClustererEnum {

    EM("EM"), SimpleKMeans("SimpleKMeans"), Canopy("Canopy"), FarthestFirst("FarthestFirst"),
    MakeDensityBasedClusterer("MakeDensityBasedClusterer"), DBScanClusterer("DbScanClusterer"),
    PAMClusterer("PAMClusterer");

    private String name;

    ClustererEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
