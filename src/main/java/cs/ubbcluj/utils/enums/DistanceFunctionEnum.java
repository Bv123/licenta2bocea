package cs.ubbcluj.utils.enums;

public enum DistanceFunctionEnum {

    ChebyshevDistance("ChebyshevDistance"), EuclideanDistance("EuclideanDistance"),
    ManhattanDistance("ManhattanDistance"), MinkowskiDistance("MinkowskiDistance");

    private String name;

    DistanceFunctionEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
