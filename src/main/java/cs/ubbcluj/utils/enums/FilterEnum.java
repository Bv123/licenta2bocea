package cs.ubbcluj.utils.enums;

public enum FilterEnum {

    Standardize("Standardize"), Discretize("Discretize"), Normalize("Normalize"),
    Resample("Resample"), PrincipalComponents("PrincipalComponents"), ReplaceMissingValues("ReplaceMissingValues"),
    NominalToBinary("NominalToBinary"), NULL("null");

    private String name;

    FilterEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
