package cs.ubbcluj.utils.enums;

public enum EvaluatorEnum {

    SilhouetteEvaluator("SilhouetteEvaluator");

    private String name;

    EvaluatorEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
