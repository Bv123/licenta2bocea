package cs.ubbcluj.utils;

import cs.ubbcluj.model.featureselection.AttributeSelector;
import cs.ubbcluj.model.program.AttributeSelectionStep;
import cs.ubbcluj.model.program.MachineLearningStep;
import cs.ubbcluj.model.program.PreprocessingStep;
import cs.ubbcluj.model.program.Program;
import cs.ubbcluj.request.ClustererRequest;
import cs.ubbcluj.utils.factory.ClustererFactory;
import cs.ubbcluj.utils.factory.DistanceFunctionFactory;
import cs.ubbcluj.utils.factory.EvaluatorFactory;
import cs.ubbcluj.utils.factory.FilterFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ProgramUtils {
    private FilterFactory filterFactory = new FilterFactory();
    private ClustererFactory clustererFactory = new ClustererFactory();
    private EvaluatorFactory evaluatorFactory = new EvaluatorFactory();
    private DistanceFunctionFactory distanceFunctionFactory = new DistanceFunctionFactory();

    public Program cloneProgram(Program parentProgram) throws Exception {
        Program program = new Program();
        PreprocessingStep preprocessingStep = new PreprocessingStep();
        if (Objects.nonNull(parentProgram.getPreprocessingStep().getFilter())) {
            preprocessingStep.setFilter(filterFactory.getObject(parentProgram.getPreprocessingStep().getFilter().getClass().getSimpleName()));
        }
        program.setPreprocessingStep(preprocessingStep);
        AttributeSelectionStep attributeSelectionStep = new AttributeSelectionStep();
        //TODO USE FACTORIES
        if (Objects.nonNull(parentProgram.getAtributeSelectionStep().getAttributeSelector())) {
            AttributeSelector newAttributeSelector = parentProgram.getAtributeSelectionStep().getAttributeSelector().getClass().newInstance();
            newAttributeSelector.setEvaluator(parentProgram.getAtributeSelectionStep().getAttributeSelector().getEvaluator().getClass().newInstance());
            newAttributeSelector.setSearch(parentProgram.getAtributeSelectionStep().getAttributeSelector().getSearch().getClass().newInstance());
            attributeSelectionStep.setAttributeSelector(newAttributeSelector);
        }
        program.setAtributeSelectionStep(attributeSelectionStep);
        MachineLearningStep newMachineLearningStep = new MachineLearningStep();
        newMachineLearningStep.setClusterer(clustererFactory.getObjectWithParams(parentProgram.getMachineLearningStep().getClusterer().getClass().getSimpleName(),
                new ClustererRequest(parentProgram.getMachineLearningStep().getClusterer().numberOfClusters())));
        program.setMachineLearningStep(newMachineLearningStep);
        program.setEvaluation(evaluatorFactory.getObject(parentProgram.getEvaluation().getClass().getSimpleName()));
        program.setDistanceFunction(distanceFunctionFactory.getObject(parentProgram.getDistanceFunction().getClass().getSimpleName()));
        return program;
    }
}
