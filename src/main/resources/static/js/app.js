var app = angular.module("licenta", []);
app.controller("FileDownloadController", function ($scope, $http) {
    $scope.asd = function () {
        console.log("asd");
    };
    $scope.clusterer = null;
    $scope.filterName = null;
    $scope.evaluator = null;
    $scope.distanceFunction = null;
    $scope.file = null;
    $scope.values = ['Pdf', 'Xlsx'];
    $scope.selected = 'Pdf';
    $scope.useKnowledgeBase = true;
    $scope.numClusters = 2;
    $scope.findOptimumNrClusters=true;
    $scope.boolean = true;
    $scope.addFile = function (uploadedFiles) {
        $scope.file = new FormData();
        $scope.file.append("file", uploadedFiles[0]);
    };
    $scope.evaluators = [];
    $scope.clusterers = [];
    $scope.filters = [];
    $scope.distanceFunctions = [];
    $scope.downloadNoAi = function () {
        if ($scope.selected === 'Pdf') {
            $scope.boolean = true;
        }
        else {
            $scope.boolean = false;
        }
        if ($scope.file === null)
            return;
        $http.post('/cluster?PDF=' + $scope.boolean + '&numClusters=' + $scope.numClusters
            + "&evaluator=" + $scope.evaluator + "&preprocessing=" + $scope.filterName
            + "&clusterer=" + $scope.clusterer + "&distanceFunction=" + $scope.distanceFunction,
            $scope.file, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                responseType: 'arraybuffer'
            }).then(function (response) {
            var file = new Blob([response.data], {
                    type: 'application/octet-stream'
                }),
                content = response.headers('Content-Disposition'),
                splitHeader = !!content ? content.split('=') : null,
                fileURL = URL.createObjectURL(file),
                a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = !!splitHeader ? splitHeader[1] : null;
            document.body.appendChild(a);
            a.click();
            a.parentNode.removeChild(a);
        });
    };

    $http.get("/algorithm/options").then(function (response) {
        $scope.clusterers = response.data.clusterers;
        $scope.clusterer = $scope.clusterers[0];
        $scope.filters = response.data.filters;
        $scope.filterName = $scope.filters[0];
        $scope.evaluators = response.data.evaluators;
        $scope.evaluator = $scope.evaluators[0];
        $scope.distanceFunctions = response.data.distanceFunctions;
        $scope.distanceFunction = $scope.distanceFunctions[0];
    });

    $scope.downloadToArff = function () {
        $http.post('csvToArff',
            $scope.file, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                responseType: 'arraybuffer'
            }).then(function (response) {
            var file = new Blob([response.data], {
                    type: 'application/octet-stream'
                }),
                content = response.headers('Content-Disposition'),
                splitHeader = !!content ? content.split('=') : null,
                fileURL = URL.createObjectURL(file),
                a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = !!splitHeader ? splitHeader[1] : null;
            document.body.appendChild(a);
            a.click();
            a.parentNode.removeChild(a);
        });
    };

    $scope.download = function () {
        if ($scope.selected === 'Pdf') {
            $scope.boolean = true;
        }
        else {
            $scope.boolean = false;
        }
        if ($scope.file === null)
            return;
        $scope.file.append('algorithmParam',JSON.stringify({
                            "minNrCluster": $scope.minNrCluster,
                            "maxNrCluster": $scope.maxNrCluster,
                            "type":"DBScanAlgorithmParam"
                        }));
        $http.post('/cluster/ai?PDF=' + $scope.boolean + '&useKnowledgeBase=' + $scope.useKnowledgeBase + '&numClusters=' + $scope.numClusters+'&findOptimumNrClusters='+$scope.findOptimumNrClusters,
            $scope.file, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity,
                responseType: 'arraybuffer'
            }).then(function (response) {
            var file = new Blob([response.data], {
                    type: 'application/octet-stream'
                }),
                content = response.headers('Content-Disposition'),
                splitHeader = !!content ? content.split('=') : null,
                fileURL = URL.createObjectURL(file),
                a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = !!splitHeader ? splitHeader[1] : null;
            document.body.appendChild(a);
            a.click();
            a.parentNode.removeChild(a);
        });
    };
});